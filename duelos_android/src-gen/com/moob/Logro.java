package com.moob;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit. 
/**
 * Entity mapped to table LOGRO.
 */
public class Logro {

    private Long id;
    /** Not-null value. */
    private String logro_name;
    private java.util.Date date;

    public Logro() {
    }

    public Logro(Long id) {
        this.id = id;
    }

    public Logro(Long id, String logro_name, java.util.Date date) {
        this.id = id;
        this.logro_name = logro_name;
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /** Not-null value. */
    public String getLogro_name() {
        return logro_name;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setLogro_name(String logro_name) {
        this.logro_name = logro_name;
    }

    public java.util.Date getDate() {
        return date;
    }

    public void setDate(java.util.Date date) {
        this.date = date;
    }

}
