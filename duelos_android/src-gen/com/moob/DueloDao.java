package com.moob;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.DaoConfig;
import de.greenrobot.dao.Property;

import com.moob.Duelo;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table DUELO.
*/
public class DueloDao extends AbstractDao<Duelo, Long> {

    public static final String TABLENAME = "DUELO";

    /**
     * Properties of entity Duelo.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property ContrincanteId = new Property(1, String.class, "contrincanteId", false, "CONTRINCANTE_ID");
        public final static Property Ganador = new Property(2, String.class, "ganador", false, "GANADOR");
        public final static Property TimeToShot = new Property(3, String.class, "timeToShot", false, "TIME_TO_SHOT");
        public final static Property MaxVelocity = new Property(4, String.class, "maxVelocity", false, "MAX_VELOCITY");
        public final static Property MinVelocity = new Property(5, String.class, "minVelocity", false, "MIN_VELOCITY");
        public final static Property Shotin = new Property(6, String.class, "shotin", false, "SHOTIN");
        public final static Property Puntuacion = new Property(7, String.class, "puntuacion", false, "PUNTUACION");
        public final static Property Date = new Property(8, java.util.Date.class, "date", false, "DATE");
    };


    public DueloDao(DaoConfig config) {
        super(config);
    }
    
    public DueloDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "'DUELO' (" + //
                "'_id' INTEGER PRIMARY KEY ," + // 0: id
                "'CONTRINCANTE_ID' TEXT," + // 1: contrincanteId
                "'GANADOR' TEXT," + // 2: ganador
                "'TIME_TO_SHOT' TEXT," + // 3: timeToShot
                "'MAX_VELOCITY' TEXT," + // 4: maxVelocity
                "'MIN_VELOCITY' TEXT," + // 5: minVelocity
                "'SHOTIN' TEXT," + // 6: shotin
                "'PUNTUACION' TEXT," + // 7: puntuacion
                "'DATE' INTEGER);"); // 8: date
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "'DUELO'";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, Duelo entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        String contrincanteId = entity.getContrincanteId();
        if (contrincanteId != null) {
            stmt.bindString(2, contrincanteId);
        }
 
        String ganador = entity.getGanador();
        if (ganador != null) {
            stmt.bindString(3, ganador);
        }
 
        String timeToShot = entity.getTimeToShot();
        if (timeToShot != null) {
            stmt.bindString(4, timeToShot);
        }
 
        String maxVelocity = entity.getMaxVelocity();
        if (maxVelocity != null) {
            stmt.bindString(5, maxVelocity);
        }
 
        String minVelocity = entity.getMinVelocity();
        if (minVelocity != null) {
            stmt.bindString(6, minVelocity);
        }
 
        String shotin = entity.getShotin();
        if (shotin != null) {
            stmt.bindString(7, shotin);
        }
 
        String puntuacion = entity.getPuntuacion();
        if (puntuacion != null) {
            stmt.bindString(8, puntuacion);
        }
 
        java.util.Date date = entity.getDate();
        if (date != null) {
            stmt.bindLong(9, date.getTime());
        }
    }

    /** @inheritdoc */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public Duelo readEntity(Cursor cursor, int offset) {
        Duelo entity = new Duelo( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1), // contrincanteId
            cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2), // ganador
            cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), // timeToShot
            cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4), // maxVelocity
            cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5), // minVelocity
            cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6), // shotin
            cursor.isNull(offset + 7) ? null : cursor.getString(offset + 7), // puntuacion
            cursor.isNull(offset + 8) ? null : new java.util.Date(cursor.getLong(offset + 8)) // date
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, Duelo entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setContrincanteId(cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1));
        entity.setGanador(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.setTimeToShot(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setMaxVelocity(cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4));
        entity.setMinVelocity(cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5));
        entity.setShotin(cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6));
        entity.setPuntuacion(cursor.isNull(offset + 7) ? null : cursor.getString(offset + 7));
        entity.setDate(cursor.isNull(offset + 8) ? null : new java.util.Date(cursor.getLong(offset + 8)));
     }
    
    /** @inheritdoc */
    @Override
    protected Long updateKeyAfterInsert(Duelo entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    /** @inheritdoc */
    @Override
    public Long getKey(Duelo entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
}
