package com.moob.frwk;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;

public class MyAnimationListener implements AnimationListener{
	View mView;
	int mModifierX,mModifierY;
	Context mContext;

	public MyAnimationListener(View v, int modifierX, int modifierY, Context c){
	    mView=v;
	    mModifierX=modifierX;
	    mModifierY=modifierY;
	    mContext=c;
	}
	public void onAnimationEnd(Animation animation) {
		// IMPORTANT !!!!!!
		mView.clearAnimation();   //else the button view and real button do not match exactly...
		

		/*String strr="left before : "+mView.getLeft();
	    Log.d("trace",strr);*/
	    
	    int[] pos={mModifierX+mView.getLeft(),mModifierY+mView.getTop(),mModifierX+mView.getRight(),mModifierY+mView.getBottom()};
	    mView.layout(pos[0],pos[1],pos[2],pos[3]);
	    /*String str="left after : "+mView.getLeft()+", top:"+mView.getTop();
	    Log.d("trace",str);*/
	}

	public void onAnimationRepeat(Animation animation) {}

	public void onAnimationStart(Animation animation) {}

	}
