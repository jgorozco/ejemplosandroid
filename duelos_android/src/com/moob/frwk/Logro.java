package com.moob.frwk;

public class Logro {
	String name;
	String info;
	String fecha;

	public Logro(String name, String info, String fecha){
		this.name = name;
		this.info = info;
		this.fecha = fecha;
	}

	@Override
	public String toString(){
		return "Logro: " + name + " info: " + info +" fecha: " + fecha;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public String getName(){
		return name;
	}
	
	public void setInfo(String info){
		this.info = info;
	}
	
	public String getInfo(){
		return info;
	}
	
	public void setFecha(String fecha){
		this.fecha = fecha;
	}
	
	public String getFecha(){
		return fecha;
	}
}
