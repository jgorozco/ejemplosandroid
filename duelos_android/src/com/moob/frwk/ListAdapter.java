package com.moob.frwk;

import java.util.ArrayList;
import com.moob.R;
import com.moob.FB.FriendsGetProfilePics;
import com.moob.FB.Utility;


import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

//http://www.ace-art.fr/wordpress/2010/07/21/tutoriel-android-partie-6-les-listview/
public class ListAdapter extends BaseAdapter {

	LayoutInflater inflater;
	ArrayList<Friend> listadoData;
	
	
	public ListAdapter(Context context, ArrayList<Friend> listadoData) {
		super();
		this.inflater = LayoutInflater.from(context);
		this.listadoData = listadoData;
		
        if (Utility.model == null) {
            Utility.model = new FriendsGetProfilePics();
        }
        Utility.model.setListener(this);
        
    
	}

	@Override
	public int getCount() {
		return listadoData.size();
	}

	@Override
	public Object getItem(int arg0) {
		return listadoData.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	

	
	private class ViewHolder {

		TextView userNick;

		TextView points;
		
		ImageView imageView;
		}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
	ViewHolder holder;

	if(convertView == null) {

	holder = new ViewHolder();

	convertView = inflater.inflate(R.layout.cellview, null);

	holder.userNick = (TextView)convertView.findViewById(R.id.userNick);

	holder.points = (TextView)convertView.findViewById(R.id.points);
	
	holder.imageView = (ImageView)convertView.findViewById(R.id.imageView);

	convertView.setTag(holder);

	} else {

	holder = (ViewHolder) convertView.getTag();

	}

	holder.userNick.setText(listadoData.get(position).getUserNick());

	holder.points.setText(listadoData.get(position).getPuntos());
	
	
    holder.imageView.setImageBitmap(Utility.model.getImage(
    listadoData.get(position).getUserId(), listadoData.get(position).getPhotoUrl()));
      
       
    return convertView;
    
	}
	
	
}
