package com.moob.frwk;

import java.util.ArrayList;

import com.moob.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ListAdapLog extends BaseAdapter {
	Context context;
	ArrayList<Logro> listaDatos;
	

	public ListAdapLog(Context context, ArrayList<Logro> listaLogros){
		this.context = context;
		this.listaDatos = listaLogros;
		
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return listaDatos.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return listaDatos.get(arg0);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		RelativeLayout rowlayout;
		if (convertView==null) {
			rowlayout = generateRowView(parent,position);                
		}
		else {
			rowlayout=(RelativeLayout) convertView;
		}
		return rowlayout;
	}

	private RelativeLayout generateRowView(ViewGroup parent, int position) {
		RelativeLayout rowlayout=(RelativeLayout) LayoutInflater.from(parent.getContext())
				.inflate(R.layout.celll, parent, false);
		
		TextView fecha = (TextView) rowlayout.findViewById(R.id.fecha);
		TextView logro = (TextView) rowlayout.findViewById(R.id.logro);
		
		Logro l = listaDatos.get(position);
		
		fecha.setText(l.getFecha());
		logro.setText(l.getName());
				
		return rowlayout;
	}
	

}
