package com.moob.frwk;

import java.util.Date;

public class General {
	Date fecha;
	String ID;
	int score;

	public General (String ID, int score, Date fecha){
		this.ID = ID;
		this.fecha = fecha;		
		this.score = score;
	}


	public String getID(){
		return ID;
	}

	public void setID(String ID){
		this.ID = ID;
	}

	public int getScore (){
		return score;
	}

	public void setScore(int score){
		this.score = score;
	}

	public Date getFecha(){
		return fecha;
	}
	
	public void setFecha(String date){
	//	Date fecha = formatFecha(date);
		this.fecha = fecha;
	}

}
