package com.moob.frwk;

import java.io.Serializable;

import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;

public class User implements Serializable{
	
	private String name;
	private String info;
	private int puntos;
	private String userId;
	
	
	
	public User(int q,String id) {
		
		
		String address = "http://duelos-android.appspot.com/getUser";
		String stringE = "{\"userId\":\""+id+"\"}";		
		String answer = UtilityServer.postData(address, stringE);
		
		try {
			JSONObject jsonObj = new JSONObject(answer);
			this.puntos = jsonObj.getInt("score");
			this.info = jsonObj.getString("nickName");
			this.name = jsonObj.getString("name");			
			this.userId = id;
			Log.d("trace","user successfully created from DB");
			
		}
		catch(Exception e){
			Log.d("trace","error creation user"+e.getMessage());
			this.name="player";
			this.info = "none";
			this.puntos=0;
			this.userId="0";
		}
	}
	
	public User(String name) {
		this.name=name;
		this.info=null;
		this.puntos=0;
	}
	public User(String name,String id,int points) {
		this.name=name;
		this.info=null;
		this.userId=id;
		this.puntos=points;
	}
	
	public User(String message,String id) {
		this.name=message;
		this.info=null;
		this.userId=id;
		this.puntos=Integer.parseInt(getPuntosFromGooAppEng(userId));
	}
	
	public User(String name ,int puntos) {
		this.name=name;
		this.puntos=puntos;
	}

	public void setInfo(String message) {
		this.info=message;
		
	}
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	

	public int getPuntos() {
		return puntos;
	}

	public void setPuntos(int puntos) {
		this.puntos = puntos;
	}

	
	
	@Override
	public String toString() {
		return "User [name=" + name + ", puntos=" + puntos + ", userId="
				+ userId + "]";
	}
public String getPuntosFromGooAppEng(String userId){
		
		String address = "http://duelos-android.appspot.com/getActualScore";
		String stringE = "{\"userId\":\""+userId+"\"}";
		String result="0";
		String answer = UtilityServer.postData(address, stringE);
		
		try {
			JSONObject jsonObj = new JSONObject(answer);
			result = jsonObj.getString("result");
		}
		catch(Exception e){
			
		}
		
		
		return result;
	}


	
	
	
}
