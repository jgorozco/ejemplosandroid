package com.moob.frwk;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.Facebook;
import com.moob.FB.Utility;


public class SocialNetworkManager {

	private boolean loggedWithFacebook=false;
	public static final String APP_ID = "292558090853692";
	
	private JSONArray friends=null;
	
	public void facebookLogin()
	{
		Utility.mFacebook = new Facebook(APP_ID);
       	Utility.mAsyncRunner = new AsyncFacebookRunner(Utility.mFacebook);
       	loggedWithFacebook=Utility.mFacebook.isSessionValid();
       	
       	
	}
	public void twitterLogin()
	{
		
	}
	
	public void postInfo(Disparo disparo)
	//para el muro fb
	{	 String msg = disparo.toString();
		 Log.d("Tests", "Testing graph API wall post");
         try {
                String response = Utility.mFacebook.request("me");
                Bundle parameters = new Bundle();
                parameters.putString("message", msg);
                parameters.putString("description", "test test test");
                response = Utility.mFacebook.request("me/feed", parameters, 
                        "POST");
                Log.d("Tests", "got response: " + response);
                if (response == null || response.equals("") || 
                        response.equals("false")) {
                   Log.v("Error", "Blank response");
                }
         } catch(Exception e) {
             e.printStackTrace();
         }
    }
	
	
	
	public ArrayList<Friend> getListaAmigos()
	{
		
		return Utility.arrayListFriend;
		
	}	
	
	public String arrayListFriendtoString() {
		
		return Utility.arrayListFriend.toString();
	}
	public boolean getLoggedWithFacebook() {
		return loggedWithFacebook;
	}
	public void setLoggedWithFacebook(boolean loggedWithFacebook) {
		this.loggedWithFacebook = loggedWithFacebook;
	}
	public Facebook getmFacebook() {
		return Utility.mFacebook;
	}
	
	public AsyncFacebookRunner getmAsyncRunner() {
		return Utility.mAsyncRunner;
	}
	
	
	protected void onActivityResult(int requestCode, int resultCode,Intent data) {
		Utility.mFacebook.authorizeCallback(requestCode, resultCode, data);
	}
	public void loggedWithFacebookUpdate() {
		loggedWithFacebook=Utility.mFacebook.isSessionValid();
		
	}
	public JSONArray getFriends() {
		return friends;
	}
	public void setFriends(JSONArray friends) throws JSONException {
		//if (!this.friends.toString().equals(friends.toString()))
		{		
			this.friends = friends;		
			JsonToArrayListFriend();
		}
	}
	
	public void JsonToArrayListFriend() throws JSONException{
		if(this.friends != null)
		{	
			ArrayList<Friend> arrayListFriend = new ArrayList<Friend>();
			int len = friends.length();
			for (int i=0;i<len;i++)
			{ 
			   JSONObject temp = friends.getJSONObject(i);
			   //Log.d("trace",temp.getString("name"));
			   arrayListFriend.add(new Friend(temp.getString("name"),temp.getString("uid"),temp.getString("pic_square")));
			}
			Utility.arrayListFriend=arrayListFriend;
		
		}
	}
	
}
