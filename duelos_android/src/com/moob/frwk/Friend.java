package com.moob.frwk;

import org.json.JSONObject;

public class Friend implements Comparable{
	String userNick;
	String userId;
	String puntos;
	String network;
	String photoUrl;
	
	public Friend(String userNick, String userId, String photoUrl) {
		super();
		this.userNick = userNick;
		this.userId = userId;
		this.photoUrl = photoUrl;
		this.network = "facebook";
		
		this.puntos = getPuntosFromGooAppEng(userId);
	}
	@Override
	public String toString() {
		return "Friend [userNick=" + userNick + ", userId=" + userId
				+ ", puntos=" + puntos + ", network=" + network + ", photoUrl="
				+ photoUrl + "]";
	}
	public String getUserNick() {
		return userNick;
	}
	public void setUserNick(String userNick) {
		this.userNick = userNick;
	}
	public String getPuntos() {
		return puntos;
	}
	public void setPuntos(String puntos) {
		this.puntos = puntos;
	}
	public String getPhotoUrl() {
		return photoUrl;
	}
	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/*
	 * 	DONE WITH PYTHIN WITH APTANA on GOOGLE APP ENGINE
		class getActualScore(webapp2.RequestHandler):
		    def post(self):
		        """
		        get
		        {"userId":"1301624521"}
		        return
		        {"result":"3454"}
		        OR
		        {"error":"errorText"}
		        """ 
	 * 
	 */
	public String getPuntosFromGooAppEng(String userId){
		
		String address = "http://duelos-android.appspot.com/getActualScore";
		String stringE = "{\"userId\":\""+userId+"\"}";
		String result="0";
		String answer = UtilityServer.postData(address, stringE);
		
		try {
			JSONObject jsonObj = new JSONObject(answer);
			result = jsonObj.getString("result");
		}
		catch(Exception e){
			
		}
		
		
		return result;
	}
	public int compareTo(Friend otherFriend) {
		if (Integer.parseInt(this.puntos)> Integer.parseInt(otherFriend.getPuntos()))
			return 1;
		else{ if (Integer.parseInt(this.puntos)< Integer.parseInt(otherFriend.getPuntos()))
			return -1;
		else return 0;}
		
	}
	@Override
	public int compareTo(Object another) {
		// TODO Auto-generated method stub
		return 0;
	} 
	
	
}
