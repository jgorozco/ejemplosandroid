package com.moob.frwk;

import java.util.ArrayList;

public class Datos {
	ArrayList<Disparo> d;
	ArrayList<Logro> l;
	ArrayList<Friend> f;
	ArrayList<User> u;
	

	public Datos (ArrayList<Disparo> d, ArrayList<Logro> l){
		
		this.d = d;
		this.l = l;
		
		
	}

	public ArrayList<Disparo> getD() {
		return d;
	}
	
	public ArrayList<User> getU() {
		return u;
	}

	public void setU(ArrayList<User> u) {
		this.u = u;
	}

	public void setD(ArrayList<Disparo> d) {
		this.d = d;
	}

	public ArrayList<Logro> getL() {
		return l;
	}

	public void setL(ArrayList<Logro> l) {
		this.l = l;
	}

	public ArrayList<Friend> getF() {
		return f;
	}

	public void setF(ArrayList<Friend> f) {
		this.f = f;
	}

}
