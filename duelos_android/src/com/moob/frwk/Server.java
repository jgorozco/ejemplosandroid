package com.moob.frwk;

public class Server extends User {
	
	private int capacity;
	private int occupation;
	private String address;
	private long ping;
	
	public Server(String name,String address){
		super(name);
		this.address = address;
		this.capacity=2;
		this.occupation=1;
		this.ping=999;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public int getOccupation() {
		return occupation;
	}

	public void setOccupation(int occupation) {
		this.occupation = occupation;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	public long getPing() {
		return ping;
	}

	public void setPing(long ping) {
		this.ping = ping;
	}

	@Override
	public String toString() {
		return "Server [capacity=" + capacity + ", occupation=" + occupation
				+ ", address=" + address + ", getName()=" + getName()
				+ ", toString()=" + super.toString() + ", getClass()="
				+ getClass() + ", hashCode()=" + hashCode() + "]";
	}
	
}
