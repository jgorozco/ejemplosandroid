package com.moob.frwk;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.util.Log;

public class Disparo implements Serializable{
	String user;
	float timeToShot;
	float maxVelocity;
	float minVelocity;
	Date fecha;
	int shotin;
	String fechaString;

	public Disparo(String user, float timeToShot, float maxVelocity,
			float minVelocity, int shotin, Date fecha) {
		super();
		this.user = user;
		this.timeToShot = timeToShot;
		this.maxVelocity = maxVelocity;
		this.minVelocity = minVelocity;
		this.shotin = shotin;
		this.fecha = fecha;
	}


	public Disparo(String fechaString, int shotin) {
		super();
		this.fechaString = fechaString;
		this.shotin = shotin;
	}


	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public float getTimeToShot() {
		return timeToShot;
	}
	public void setTimeToShot(float timeToShot) {
		this.timeToShot = timeToShot;
	}
	public float getMaxVelocity() {
		return maxVelocity;
	}
	public void setMaxVelocity(float maxVelocity) {
		this.maxVelocity = maxVelocity;
	}
	public float getMinVelocity() {
		return minVelocity;
	}
	public void setMinVelocity(float minVelocity) {
		this.minVelocity = minVelocity;
	}
	public int getShotin() {
		return shotin;
	}
	public void setShotin(int shotin) {
		this.shotin = shotin;
	}
	public Date getFecha(){
		return fecha;
	}
	public String getFechaString(){
		return fechaString;
	}
	public Date formatFecha(String fecha){
		SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-ddThh:mm:ss.SSSSSS");
		Date date = null;
		try {
			date = formatoDelTexto.parse(fecha);
		} 
		catch (ParseException ex) {
			ex.printStackTrace();
		}
		Log.d("fecha bien", date.toString());

		return date;

	}

	public void setFecha(String date){
		Date fecha = formatFecha(date);
		this.fecha = fecha;
	}
	@Override
	public String toString() {
		return "Disparo [user=" + user + ", timeToShot=" + timeToShot
				+ ", maxVelocity=" + maxVelocity + ", minVelocity="
				+ minVelocity + ", shotin=" + shotin + "]";
	}

}
