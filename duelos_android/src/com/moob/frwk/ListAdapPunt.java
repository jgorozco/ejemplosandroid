package com.moob.frwk;

import java.util.ArrayList;

import com.moob.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ListAdapPunt extends BaseAdapter {
	Context context;
	ArrayList<Disparo> listaDatos;
	
	public ListAdapPunt(Context context, ArrayList<Disparo> listaPuntos){
		this.context = context;
		this.listaDatos = listaPuntos;		
	}

	@Override
	public int getCount() {
		
		return listaDatos.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return listaDatos.get(arg0);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		RelativeLayout rowlayout;
		if (convertView==null) {
			rowlayout = generateRowView(parent,position);                
		}
		else {
			rowlayout=(RelativeLayout) convertView;
		}
		return rowlayout;
	}

	private RelativeLayout generateRowView(ViewGroup parent, int position) {
		RelativeLayout rowlayout=(RelativeLayout) LayoutInflater.from(parent.getContext())
				.inflate(R.layout.cellp, parent, false);
		
		TextView fecha = (TextView)rowlayout.findViewById(R.id.date);
		TextView puntos = (TextView) rowlayout.findViewById(R.id.points);
		Disparo d = listaDatos.get(position);
		
		fecha.setText(d.getFechaString());
		puntos.setText(String.valueOf(d.getShotin()));
	
		return rowlayout;
	}
	

}
