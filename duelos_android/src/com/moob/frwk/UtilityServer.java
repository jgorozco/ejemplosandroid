package com.moob.frwk;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Application;
import android.util.Log;

public class UtilityServer extends Application{

	public UtilityServer()  {
		// TODO Auto-generated constructor stub
	}
	public static String getData(String address) throws Exception{
		HttpClient httpclient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(address);
		String rep="";
		
			HttpResponse response = httpclient.execute(httpGet);
			StringBuilder reponse = inputStreamToString(response.getEntity().getContent());
			rep = reponse.toString();
		
		
		return rep;
	}
	public static String postData(String address, String stringE) {
        // Create a new HttpClient and Post Header
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(address);
        String rep="";
        try {
            // Add your data
        	httppost.setHeader("Content-type", "application/json");
        	//Log.d("trace",stringE);
        	httppost.setEntity(new StringEntity(stringE));
        	httppost.setHeader("Accept", "application/json");
            HttpResponse response = httpclient.execute(httppost);
            StringBuilder reponse = inputStreamToString(response.getEntity().getContent());
            rep = reponse.toString();
            //Log.d("trace",rep);
            
        } catch (ClientProtocolException e) {
        	Log.d("trace","error1 "+e.getMessage());
        } catch (IOException e) {
        	Log.d("trace","error2 "+e.getMessage());
        }
        return rep;
    }  
	
	 // Fast Implementation
    private static StringBuilder inputStreamToString(InputStream is) throws IOException {
        String line = "";
        StringBuilder total = new StringBuilder();
        
        // Wrap a BufferedReader around the InputStream
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));

        // Read response until the end
        while ((line = rd.readLine()) != null) { 
            total.append(line); 
        }
        
        // Return full string
        return total;
    }
}
