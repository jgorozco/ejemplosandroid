package com.moob.BT;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import com.moob.FB.Utility;
import com.moob.frwk.Disparo;
import com.moob.frwk.Server;
import com.moob.frwk.User;



import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;

//******************* SERVEUR *********************
//@TargetApi(10)
public class AcceptThread extends Thread {
	   private final BluetoothServerSocket mmServerSocket;
	   private BluetoothAdapter bluetoothAdapter;
	   private ObjectInputStream  mmInStream=null;
       private ObjectOutputStream  mmOutStream=null;
	   private String message;
	   private boolean open=false;
	   private boolean playing=false;
	   private boolean waitingShoot=false;
	   private boolean sentShoot=false;
	   private Handler handler;
	   private User user = null;
	   private Server server=null;
	   private int sdkVersion = 0;
	   private Disparo clientShoot=null;
	   private Disparo serverShoot=null;	
	   public AcceptThread() {
		   
		   Log.d("trace","Thread Serveur constructeur");
	       BluetoothServerSocket tmp = null;
	       sdkVersion = android.os.Build.VERSION.SDK_INT; // e.g. sdkVersion := 8;
	       try { bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
	       Log.d("trace","bt adapter"+bluetoothAdapter.toString());
	       /*
		       if (sdkVersion>8) //2.3 and more
		           tmp = socketSDKversion8plus();
		    	   else
		    	   tmp = socketSDKversion8andLess();*/
	       tmp =bluetoothAdapter.listenUsingRfcommWithServiceRecord("jeu", Utility.uuid);
	       } catch (IOException e) { Log.d("trace","error BTadapter : "+e.getMessage());}
	       mmServerSocket = tmp;
	       server = new Server(bluetoothAdapter.getName(),bluetoothAdapter.getAddress());
	       Log.d("trace","creation server : "+server.toString());
	   }
	   
	   @TargetApi(10)
	   public BluetoothServerSocket socketSDKversion8plus() throws IOException{
		   BluetoothServerSocket tmp = null;
		   tmp = bluetoothAdapter.listenUsingInsecureRfcommWithServiceRecord("jeu", Utility.uuid);
		   Log.d("trace","tmp plus8: "+tmp.toString());
		   return tmp;
	   }
	   public BluetoothServerSocket socketSDKversion8andLess() throws IOException{
		   BluetoothServerSocket tmp = null;
		   tmp =bluetoothAdapter.listenUsingRfcommWithServiceRecord("jeu", Utility.uuid);
		   Log.d("trace","tmp less8: "+tmp.toString());
		   return tmp;
	   }
	   
	   @Override
	public void run() {
	       BluetoothSocket socket = null;
	       
	       while (true) {
	    	   Log.d("trace","Thread Serveur try accecpt");
	           try {
	               socket = mmServerSocket.accept();
	               open=true;
	               Log.d("trace","Thread Serveur en attente");
	           } catch (IOException e) {
	        	   Log.d("trace","Erreur thread serveur"+e.getMessage());
	               break;
	           }

	           if (socket != null) {
	        	   Log.d("trace","Thread Serveur connection etablie");
	        	   
	               manageConnectedSocket(socket);
	               try {
					mmServerSocket.close();
				} catch (IOException e) {
					Log.d("trace","Erreur thread serveur"+e.getMessage());
					e.printStackTrace();
				}
	               break;
	           }
	       }
	   }

	   private void manageConnectedSocket(BluetoothSocket socket) {
		   
		 //3. get Input and Output streams
		   try {
			mmOutStream = new ObjectOutputStream(socket.getOutputStream());
			mmOutStream.flush();
			mmInStream = new ObjectInputStream(socket.getInputStream());
		   } catch (IOException e1) {
			
			Log.d("trace",e1.getMessage());
		   }
		   
		
           Log.d("trace","SERVEUR CONNECTE !!");
           

       
         //4. The two parts communicate via the input and output streams
			while(open){
				sentShoot=false;
				try{
					//**************************************************
					// WAITING FOR DETECTION
					Log.d("trace","serveur waiting message");
					message = (String)mmInStream.readObject();
					Log.d("trace","MESSAGE RECU SERVEUR : "+message);
					//user add to view
					if(message.equals("detection")){
						Message msg = new Message();
						msg.obj=message;						
						msg.what=0; 
						handler.sendMessage(msg);
						
						//**************************************************
						// STOP AND RESTART
						//Send to the client a server object witch is serializable via the OutputStream
		 		   		sendMessage(server);
		 		   		
			 		   	msg = new Message();
						msg.what=1;
		 		   		handler.sendMessage(msg);
						}
					else{
						if(message.equals("startGame")){
							Message msg = new Message();
							msg.obj=message;						
							msg.what=2; 
							handler.sendMessage(msg);
							playing=true;
						}
					}
				}
				catch(ClassNotFoundException classnot){
					Log.d("trace","Data received in unknown format");
					open=false;
					playing=false;
				} catch (OptionalDataException e) {
					Log.d("trace","error serveur????"+e.getMessage());
					open=false;
					playing=false;
				} catch (IOException e) {
					Log.d("trace","error serveur??"+e.getMessage());
					open=false;
					playing=false;
				}
			
			
			while(playing&&open){
				sentShoot=false;
				Log.d("trace","SERVER THREAD WHILE PLAYING");
				//1 get a random number
				//2 send it to the client
				// sleep then vibrate
				//while
					//wait for serverUser to shoot OR a shoot from the client
				
				//random to send a signal to make both devices vibrating
				int random = getRandomInt();
				
				//**************************************************
				//Send the random value to the client
				
				sendMessage(String.valueOf(random));				
				//sleep the random time
				
				try {
					sleep(random*1000);
				} catch (InterruptedException e) {
					Log.d("trace","SERVER thread SLEEP FAIL");
					playing=false;
				}
				//send value to GameClientActivity because 
				//Vibrate code must be called with a reference to a Context
				Message msg2 = new Message();
				msg2.obj="nothing";						
				msg2.what=3; 
				handler.sendMessage(msg2);
				
				
				waitingShoot=true;
				//WAIT SHOT from him self AND from the client
				while(waitingShoot){
					//did the client shooted me ?
					try{
						Log.d("trace","SERVER WAITING FOR SHOT");
						clientShoot = (Disparo)mmInStream.readObject();
						Log.d("trace","SHOT RECEIVED");
						//***************
						Message msg = new Message();
						msg.what=6;
						if(clientShoot.getUser().equals("fake")){						
							//I receive a fake shot to start next round and i won
							msg.obj="fake";						
							 
						}
						else{
							//I receive a shot from real user, so i lost and i make death sound
							msg.obj="lose";	
						}
						handler.sendMessage(msg);
						//***************
						waitingShoot=false;
						
					}
					catch(ClassNotFoundException classnot){
						Log.d("trace","555t");waitingShoot=false;open=false;
						
					} catch (OptionalDataException e) {
						Log.d("trace","666");waitingShoot=false;open=false;
						
					} catch (IOException e) {
						Log.d("trace","777");	
						waitingShoot=false;open=false;
						
					}
				
					//after shoot and being shooted redo
					
					
				}//end while waitingShoot
				
				
				//TODO : here you have been shoot then you send a fake shoot to play the next round
				if(!sentShoot){
					Message msg = new Message();
					msg.obj="nothing";						
					msg.what=4; 
					handler.sendMessage(msg);
					//WAITING FOR USER TO PRESS NEXT ROUND
					synchronized(this) {
				          try {
				            wait();
				          } catch(InterruptedException e) {
				            throw new RuntimeException(e);
				          }
					}
					//********************************
					Disparo disp = new Disparo("fake", 0, 0,0, 0, null);
					sendMessage(disp);
					
					
				}
				else{
					Log.d("trace","SERVER SHOT SENT");
				}
				
			}//end while playing
			}//end while open
 		  
	}
	 
	 

	public void setSentShoot(boolean sentShoot) {
		this.sentShoot = sentShoot;
	}

	private int getRandomInt() {
		 int lower = 1; 
		 int higher = 5; 

		 return ((int)(Math.random() * (higher+1-lower)) + lower); 
	}

	public void sendMessage(String msg)
	 //http://docs.oracle.com/javase/tutorial/essential/io/objectstreams.html
		{
			try{
				
				mmOutStream.writeObject(msg);
				mmOutStream.flush();
				Log.d("trace","server envois >" + msg);
			}
			catch(IOException ioException){
				Log.d("trace","ERROR SEND MESSAGE"+ioException.getMessage());
			}
		}
	 
	 public void sendMessage(Object obj){ 
	 //http://developer.android.com/reference/java/io/ObjectOutputStream.html#write(byte[], int, int)
			
		 	try{
				mmOutStream.writeObject(obj);
				mmOutStream.flush();
				Log.d("trace","client envois : "+obj.toString());
			}
			catch(IOException ioException){
				Log.d("trace","client ERROR SEND MESSAGE>"+ioException.getMessage());
			}
		}
	 
	public void cancel() {
		open=false;
		
	       try {
	    	   
	    	   mmServerSocket.close();
	    	   mmInStream.close();
	    	   mmOutStream.close();	    	   
	           
	           Log.d("trace","CLOSE Serveur");
	       } catch (IOException e) { 
	    	   Log.d("trace","CLOSE ERROR"+e.getMessage());
	       }
	   }
	
	public void stopServeur(View v){
    	try{
    		Log.d("trace","stop serveur");
    		this.cancel();
    		
    		}
    	
    	catch (Exception e){Log.d("trace","stop serveur CATCH"+e.getMessage());}
    }
	
	public void stopServeur(){
    	try{
    		Log.d("trace","stop serveur");
    		this.cancel();
    		
    		}
    	
    	catch (Exception e){Log.d("trace","stop serveur CATCH"+e.getMessage());}
    }
	
	public void setHandler(Handler handler) {
		 this.handler=handler;
		
	}
	
	}
//******************* FIN SERVEUR *********************
