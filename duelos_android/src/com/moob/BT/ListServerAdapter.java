package com.moob.BT;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.moob.R;
import com.moob.frwk.Server;

//http://www.ace-art.fr/wordpress/2010/07/21/tutoriel-android-partie-6-les-listview/
public class ListServerAdapter extends BaseAdapter {

	LayoutInflater inflater;
	ArrayList<Server> listadoData;
	
	
	public ListServerAdapter(Context context, ArrayList<Server> listadoData) {
		super();
		this.inflater = LayoutInflater.from(context);
		this.listadoData = listadoData;
		
      
  
	}

	@Override
	public int getCount() {
		return listadoData.size();
	}

	@Override
	public Object getItem(int arg0) {
		return listadoData.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	

	
	private class ViewHolder {

		TextView serverName;

		TextView serverAddress;
		
		TextView serverOccupation;
		
		TextView ping;
		}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
	ViewHolder holder;

	if(convertView == null) {

	holder = new ViewHolder();

	convertView = inflater.inflate(R.layout.cellviewserver, null);

	holder.serverName = (TextView)convertView.findViewById(R.id.serverName);

	holder.serverAddress = (TextView)convertView.findViewById(R.id.serverAddress);
	
	holder.serverOccupation = (TextView)convertView.findViewById(R.id.serverOccupation);
	
	holder.ping = (TextView)convertView.findViewById(R.id.ping);

	convertView.setTag(holder);

	} else {

	holder = (ViewHolder) convertView.getTag();

	}
	holder.serverName.setText(listadoData.get(position).getName());

	holder.serverAddress.setText(listadoData.get(position).getAddress());
	
	holder.serverOccupation.setText(String.valueOf(listadoData.get(position).getOccupation())+"/"+String.valueOf(listadoData.get(position).getCapacity()));
	
	holder.ping.setText(String.valueOf(listadoData.get(position).getPing()));
	
    
     
  return convertView;
  
	}
	
	
}
