package com.moob.BT;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.io.OutputStream;
import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;

import com.moob.FB.Utility;
import com.moob.frwk.Disparo;

//http://stackoverflow.com/questions/5550670/benefit-of-using-parcelable-instead-of-serializing-object
public class ClientGame extends Thread{

   private final BluetoothSocket mmSocket;
   private final BluetoothDevice mmDevice;
   private ObjectOutputStream out;
   private ObjectInputStream in;
   private boolean open = false;
   private int sdkVersion = 0;
   private BluetoothAdapter bluetoothAdapter;
   private Handler handler2;
   private Disparo shoot=null;
   private boolean sentShoot=false;
   private boolean handlerSet=false;
   private boolean waitingShoot=false;
  
	   
	public ClientGame(BluetoothDevice device) {
		
		//version of android OS
        sdkVersion = android.os.Build.VERSION.SDK_INT; // e.g. sdkVersion := 8;
        
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        
		BluetoothSocket tmp = null;
	    mmDevice = device;
	    InputStream tmpIn = null;
        OutputStream tmpOut = null;
        
	       try {
	    	   //http://stackoverflow.com/questions/5885438/bluetooth-pairing-without-user-confirmation
	    	   // Insecure connection with Android 2.3.3 and further
	    	   if (sdkVersion>8) //2.3 and more
	           tmp = socketSDKversion8plus(device);
	    	   else
	    	   tmp = socketSDKversion8andLess(device);
	       } 
	       catch (IOException e) 
    	       	{
    	    	Log.d("trace","CLIENTGAME thread error"+e.getMessage()); 
    	    	}
	       mmSocket = tmp;
	   }
		
	

   @TargetApi(10)
   public BluetoothSocket socketSDKversion8plus(BluetoothDevice device) throws IOException{
	   BluetoothSocket tmp = null;
	   tmp = device.createInsecureRfcommSocketToServiceRecord(Utility.uuid);
	   return tmp;
   }
   public BluetoothSocket socketSDKversion8andLess(BluetoothDevice device) throws IOException{
	   BluetoothSocket tmp = null;
	   tmp = device.createRfcommSocketToServiceRecord(Utility.uuid);
	   return tmp;
   }
   
	@Override
	public void run(){
		Log.d("trace","run of ClientGame");	
	       try {
	    	  mmSocket.connect() ;	           
	       } catch (IOException connectException) {	    	   
	           try {
	               mmSocket.close();
	               Log.d("trace","socket client close ClientGame : "+connectException.getMessage()); 
	           } catch (IOException closeException) {
	        	   	Log.d("trace","CLIENT close Error"+closeException.getMessage());	        	   	
	           		}	           
	           Log.d("trace","IOException : "+connectException.getMessage());
	           return;
	       }
	      
	       try{
	     	//2. get Input and Output streams
   			out = new ObjectOutputStream(mmSocket.getOutputStream());
   			out.flush();
   			in = new ObjectInputStream(mmSocket.getInputStream());
   			Log.d("trace","CLIENT CONNECTE debut fonction");
	       	}
	       catch(Exception e){
	    	   Log.d("trace","IOSTREAM ERROR"+e.getMessage());
	       }
	       
	       this.manageConnectedSocket(mmSocket);
	       
   }
	
	private void manageConnectedSocket(BluetoothSocket mmSocket){
		Log.d("trace","manageConnectedSocket of ClientGame");
		sendMessage("startGame");
		open=true;
		while(open){
			sentShoot=false;
					// Ingame
					//-1  wait for random
					//-2  sleep
					//-3 vibrate
					//-4 while
						//- wait a user shoot OR a message from client that shooted
					
		
			//**************************************************
			//read random value
			Log.d("trace","Client thread reading random value");
			try{
				long random = Long.parseLong((String)in.readObject());
				
				Log.d("trace","Client thread random value "+random);
				try {
					sleep(random*1000);
				} catch (InterruptedException e) {
					Log.d("trace","Client thread SLEEP FAIL");
					e.printStackTrace();
				}
			//send value to GameClientActivity because 
			//Vibrate code must be called with a reference to a Context
				Message msg = new Message();				
				msg.obj=random;						
				msg.what=0; 
				boolean sent=false;
				if (handlerSet)
					sent=handler2.sendMessage(msg);
				else 
					Log.d("trace","Client thread HANDLER NOT SET");
				Log.d("trace","Client thread message sent"+sent);
				
				
			}
			catch(ClassNotFoundException classnot){
				Log.d("trace","Data received in unknown format");
				open=false;
			} catch (OptionalDataException e) {
				Log.d("trace","error serveur????"+e.getMessage());
				open=false;
			} catch (IOException e) {
				Log.d("trace","error serveur??"+e.getMessage());
				open=false;
			}
			waitingShoot=true;
			//4
			while(waitingShoot){		
				//4.1 Did i shoot ?
				try{				
					//reading
					Disparo disparo =(Disparo)in.readObject();
					waitingShoot=false;
					//send message to Toast user that he have been shooted
					Message msg = new Message();				
					msg.obj=disparo.getUser();						
					msg.what=1;		
					//missing ?
					handler2.sendMessage(msg);
					Log.d("trace","SHOT SERVER RECEIVED");
					
				}
				catch(ClassNotFoundException classnot){
					Log.d("trace","111");waitingShoot=false;open=false;
					
				} catch (OptionalDataException e) {
					Log.d("trace","222"+e.getMessage());waitingShoot=false;open=false;
					
				} catch (IOException e) {
					Log.d("trace","333"+e.getMessage());waitingShoot=false;open=false;
					
				}
			}
			//I send fake shoot to reloop again
			//because the server is waiting for a shoot anw
		
			if(!sentShoot){
				
				
				Message msg = new Message();
				msg.obj="nothing";						
				msg.what=2; 
				handler2.sendMessage(msg);
				
				//waiting next round
				Log.d("trace","WAIT");
				synchronized(this) {
			          try {
			            wait();
			          } catch(InterruptedException e) {
			            throw new RuntimeException(e);
			          }
				}
				Log.d("trace","AFTER WAIT");
				
				//
				Disparo disp = new Disparo("fake", 0, 0, 0, 0, null);
				sendMessage(disp);
				Message msg3 = new Message();
				msg3.obj="nothing";						
				msg3.what=4; 
				handler2.sendMessage(msg3);
				Log.d("trace","SEND  SHOOT sentShoot is false");
			}
			else{
				Log.d("trace","SEND  SHOOT sentShoot is true");
			}
		}
	}
	
	public void setSentShoot(boolean sentShoot) {
		this.sentShoot = sentShoot;
	}

	
	public void sendMessage(String msg){    		
			try{
				out.writeObject(msg);
				out.flush();
				Log.d("trace","client envois >" + msg);
			}
			catch(IOException ioException){
				Log.d("trace","client ERROR SEND MESSAGE>"+ioException.getMessage());
			}
		}
	
	public void sendMessage(Object obj){ 
		 //http://developer.android.com/reference/java/io/ObjectOutputStream.html#write(byte[], int, int)
				try{
					out.writeObject(obj);
					out.flush();
					Log.d("trace","client envois : "+obj.toString());
				}
				catch(IOException ioException){
					Log.d("trace","client ERROR SEND MESSAGE>"+ioException.getMessage());
				}
			}
	
	public void cancel() {
			open = false;
			waitingShoot=false;
  	       try {
  	    	   out.close();
  	    	   in.close();
  	    	   mmSocket.close();
  	           Log.d("trace","client close succes");
  	       } catch (IOException e) { 
  	    	   Log.d("trace","CLIENT close (cancel) Error"+e.getMessage());
  	       }
  	   }
	  
	public void stopServeur(View v){
	    	try{
	    		Log.d("trace","stop clientGame");
	    		this.cancel();
	    		
	    		}
	    	
	    	catch (Exception e){Log.d("trace","stop clientGame CATCH"+e.getMessage());}
	    }
		
	public void stopServeur(){
	    	try{
	    		Log.d("trace","stop clientGame");
	    		this.cancel();	    		
	    		}
	    	
	    	catch (Exception e){Log.d("trace","stop clientGame CATCH"+e.getMessage());}
	    }
	
	public void setHandler(Handler handler2) {
		 this.handler2=handler2;
		 this.handlerSet=true;
		
	}
	

	
}
