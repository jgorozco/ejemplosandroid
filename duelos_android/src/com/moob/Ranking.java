package com.moob;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.moob.FB.Utility;
import com.moob.frwk.Datos;
import com.moob.frwk.Disparo;
import com.moob.frwk.Friend;
import com.moob.frwk.ListAdapClasi;
import com.moob.frwk.ListAdapLog;
import com.moob.frwk.ListAdapPunt;
import com.moob.frwk.ListAdapter;
import com.moob.frwk.Logro;
import com.moob.frwk.User;
import com.moob.frwk.UtilityServer;

import android.net.ParseException;
import android.net.http.AndroidHttpClient;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.Toast;
import android.widget.TabHost.TabSpec;
import android.app.Activity;



public class Ranking extends Activity {

	public ListView puntos;
	public ListView logros;
	
	//ranking
	public ListView ranking;
	
	//clasificacion amigos
	public ArrayList<Friend> listadoData;
	public ListView listranking;
	public ListAdapter adapter;
	
	public ArrayList<Logro> listaLogros;
	public ArrayList<Disparo> listaPuntos;
	public ArrayList<User> listaUsers;
	
	public ListAdapPunt adapterP;
	public ListAdapLog adapterL;
	public ListAdapClasi adapterClassificacion;
	
	public Datos datos;
	public Utility utility;
	public static AndroidHttpClient httpclient = null;
	private boolean rankingLoad;
	
	


	//@SuppressWarnings("unchecked")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.rankingmain);

		rankingLoad=false;
		puntos = (ListView)findViewById(R.id.puntos);
		logros = (ListView)findViewById(R.id.logros);
		//ranking
		ranking = (ListView)findViewById(R.id.amigos);
		//amigos !!!
		listranking = (ListView)findViewById(R.id.clasificacion);
		
		
		//init
		listaUsers = new ArrayList<User>();
		listaLogros = new ArrayList<Logro>();
		listaPuntos = new ArrayList<Disparo>();
		datos = new Datos(listaPuntos, listaLogros);
		leerJSON();
		if (datos.getD().size() == 0){
			Log.d("trace","disparos empty");
		}
		else{
			Log.d("trace","disparos  not empty");
		}
		
		listaPuntos = datos.getD();
		listaLogros = datos.getL();
		
		

		adapterP = new ListAdapPunt(getApplicationContext(), listaPuntos);
		adapterL = new ListAdapLog (getApplicationContext(), listaLogros);
		

		puntos.setAdapter(adapterP);
		logros.setAdapter(adapterL);
		
		
		
		
		

		TabHost tabHost = (TabHost)findViewById(R.id.tabhost);
		tabHost.setup();
		TabSpec spec1 = tabHost.newTabSpec("Mis Puntos");
		spec1.setContent(R.id.tab1);
		spec1.setIndicator("Mis Puntos");

		TabSpec spec2 = tabHost.newTabSpec("Mis Logros");
		spec2.setIndicator("Mis Logros");
		spec2.setContent(R.id.tab2);
		
		TabSpec spec3 = tabHost.newTabSpec("Clasificacion");
		spec3.setIndicator("Clasificacion");
		spec3.setContent(R.id.tab3);
		
		TabSpec spec4 = tabHost.newTabSpec("Amigos");
		spec4.setIndicator("Amigos");
		spec4.setContent(R.id.tab4);
		

		tabHost.addTab(spec1);
		tabHost.addTab(spec2);
		tabHost.addTab(spec3);
		tabHost.addTab(spec4);
		
		if (Utility.arrayListFriend != null) {
			Collections.sort(Utility.arrayListFriend, new CustomComparator());
			listadoData=Utility.arrayListFriend;
			Log.d("trace","length friend array : "+String.valueOf(listadoData.size()));
			adapter=new ListAdapter(getApplicationContext(),listadoData);
			listranking.setAdapter(adapter);
			
			}
			else{
				listadoData = new ArrayList<Friend>();
				Log.d("trace","listadoData null");
				
			}
	//LISTENER TABHOST
	tabHost.setOnTabChangedListener(new tabListner());
		

	}
	
	public class tabListner implements TabHost.OnTabChangeListener{

		@Override
		public void onTabChanged(String tabId) {
			if (tabId.equals("Amigos")) {
				if(Utility.mFacebook != null){
					if(Utility.mFacebook.isSessionValid())
					{
						if (Utility.arrayListFriend != null) 
							{				
							Collections.sort(Utility.arrayListFriend, new CustomComparator());
							listadoData=Utility.arrayListFriend;
							adapter=new ListAdapter(getApplicationContext(),listadoData);
							listranking.setAdapter(adapter);
							}
						else
							Toast.makeText(getApplicationContext(), "Still loading try un a few second", Toast.LENGTH_SHORT).show();
					}	
					
				}
				else{
					Toast.makeText(getApplicationContext(), "You must connect to Facebook", Toast.LENGTH_SHORT).show();
					}
					
			}
			if(tabId.equals("Clasificacion")) {
				if(listaUsers.isEmpty()){
					rankingLoad = false;
				}
				//ranking of everybody
				if (! rankingLoad /*&& !Utility.userUID.equals("")*/){
				listaUsers=getRanking();
					if (!listaUsers.isEmpty()){
						Log.d("trace","ranking"+ String.valueOf(listaUsers.size()));
						adapterClassificacion = new ListAdapClasi(getApplicationContext(),listaUsers);
						ranking.setAdapter(adapterClassificacion);
						rankingLoad=true;
					}
					else{
						Toast.makeText(getApplicationContext(), "Error from server", Toast.LENGTH_SHORT).show();
						rankingLoad=false;
					}
				}
			}
		}

		
		
	}
	public class CustomComparator implements Comparator<Friend> {
		//http://stackoverflow.com/questions/890254/how-can-i-sort-this-arraylist-the-way-that-i-want
	    @Override
	    public int compare(Friend o1, Friend o2) {
	        return -1*o1.getPuntos().compareTo(o2.getPuntos());
	    }
	}
	
	
	public String postData(String address, String stringE) {
        // Create a new HttpClient and Post Header
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(address);
        String rep="";
        try {
            // Add your data
        	httppost.setHeader("Content-type", "application/json");
        	//Log.d("trace",stringE);
        	httppost.setEntity(new StringEntity(stringE));
        	httppost.setHeader("Accept", "application/json");
            HttpResponse response = httpclient.execute(httppost);
            StringBuilder reponse = inputStreamToString(response.getEntity().getContent());
            rep = reponse.toString();
            //Log.d("trace",rep);
            
        } catch (ClientProtocolException e) {
        	Log.d("trace","error1 "+e.getMessage());
        } catch (IOException e) {
        	Log.d("trace","error2 "+e.getMessage());
        }
        return rep;
    }  
	
	 // Fast Implementation
    private StringBuilder inputStreamToString(InputStream is) throws IOException {
        String line = "";
        StringBuilder total = new StringBuilder();
        
        // Wrap a BufferedReader around the InputStream
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));

        // Read response until the end
        while ((line = rd.readLine()) != null) { 
            total.append(line); 
        }
        
        // Return full string
        return total;
    }

    private ArrayList<User> getRanking() {
    	String url = "http://duelos-android.appspot.com/getRanking";
    	ArrayList<User> Users = new ArrayList<User> ();
    	
    	String data="";
		try {
			data = UtilityServer.getData(url);
			Log.d("trace",data);
			
			
			JSONObject jsonObj = new JSONObject(data);
			JSONArray jsonArray = jsonObj.getJSONArray("ranking");;
			
			if(jsonArray.length() > 0){
				
				for (int i=0;i<jsonArray.length();i++)
				{ 
				   JSONObject temp = jsonArray.getJSONObject(i);
				   Integer mScore = Integer.parseInt(temp.getString("score"));
				   String mName="no nickName";
				   try{
					   mName = temp.getString("name");
				   }
				   catch(Exception e){Log.d("trace", "catch reanking lecture "+e.getMessage());}
				   User u = new User(mName,mScore);
				   Users.add(u);
					}//end for
				
				} 
		} catch (JSONException e) {
			Log.d("trace", "catch reanking"+e.getMessage());
		}
		catch (Exception e1) {
			Log.d("trace","getData error "+e1.getMessage());
		}
		
		return Users;
	}
    
	public void leerJSON(){
		String url = "http://duelos-android.appspot.com/getScore";

		//	String leer = getStringFromAssetFile(getApplicationContext());
		
		String userID = Utility.userUID;
		if (!userID.equals("")){
		String jsonSent="{\"userId\":\""+userID+"\"}";
		Log.d("trace","json : "+jsonSent);
		String data = postData(url,jsonSent);
		//Log.d("trace",data);
		
		try {
			JSONObject jsonObj = new JSONObject(data);
			Log.d("trace","***********length : "+String.valueOf(jsonObj.length()));
			JSONArray jsonArray = jsonObj.getJSONArray("result");
			Log.d("trace","++++++++++length : "+String.valueOf(jsonArray.length()));
			
			/*
			JSONObject temp = jsonArray.getJSONObject(0);
			String datee = temp.getString("date");
			String scoree = temp.getString("score");
			Log.d("trace",datee+"  "+scoree);
			*/
			
			
			if(jsonArray.length() != 0)
				{	
					ArrayList<Disparo> arrayListDisparo = new ArrayList<Disparo>();
					
					for (int i=0;i<jsonArray.length();i++)
					{ 
					   JSONObject temp = jsonArray.getJSONObject(i);
					   Integer mScore = Integer.parseInt(temp.getString("score"));
					   
					   String mDate =  temp.getString("date");
					   
					   
					   
					   if (!mDate.equals(null) && mScore != null){
						   //https://developers.google.com/appengine/docs/python/ndb/properties#Date_and_Time
						   //http://developer.android.com/reference/java/text/SimpleDateFormat.html
						   String targetDateStr="date";
						   SimpleDateFormat sourceFormat  = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS");
						   SimpleDateFormat targetFormat = new SimpleDateFormat("dd/MM/yy HH:mm");
						   try {
						        Date sourceDate = sourceFormat.parse(mDate);
						        targetDateStr = targetFormat.format(sourceDate);
						    } catch (ParseException e) {
						        Log.d("trace",e.getMessage());
						    }
						   arrayListDisparo.add(new Disparo(targetDateStr,mScore));
						   /*
						   mDate = mDate.substring(0, 10);
						   arrayListDisparo.add(new Disparo(mDate,mScore));
						   Log.d("trace","loop : "+String.valueOf(i));*/
					   
					   }
					}
					datos.setD(arrayListDisparo);			
					Log.d("trace","SIZE OF DISPARO FROM REQUEST HTTP : "+String.valueOf(arrayListDisparo.size()));
				}
				
			else{
				Log.d("trace","json array empty");
			}
			}
			
		catch(Exception e){Log.d("trace","CATCH RAISED"+e.getMessage()); }
		
		}
		else{
			Log.d("trace","id is null");
		}
		
	}
}
