package com.moob;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.Random;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;
import com.facebook.android.R;
import com.facebook.android.Util;
import com.moob.FB.BaseRequestListener;
import com.moob.FB.FriendsList;
import com.moob.FB.LoginButton;
import com.moob.FB.SessionEvents;
import com.moob.FB.SessionStore;
import com.moob.FB.SessionEvents.AuthListener;
import com.moob.FB.SessionEvents.LogoutListener;
import com.moob.FB.Utility;
import com.moob.Sound.SoundManager;
import com.moob.frwk.Disparo;
import com.moob.frwk.MyAnimationListener;
import com.moob.frwk.SocialNetworkManager;
import com.moob.frwk.UtilityServer;
import com.moob.frwk.User;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

public class LogginActivity extends Activity{
	
	
	private LoginButton mLoginButton;
	private SocialNetworkManager socialNetworkManager;
	private Context context;
	private Button mPostButton ;
	private Button mRankingButton,test;
	ProgressDialog dialog;
	private String graph_or_fql;
	private JSONArray myJsonArray;
	private float X,Y;
	private int i=0;	
	private int Xorigin,Yorigin;
	private boolean firstClick=true;  
	private int width,height;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.logginactivitymenu);
		
	
		//sound
		//Create, Initialise and then load the Sound manager
        SoundManager.getInstance();
        SoundManager.initSounds(this);
        SoundManager.loadSounds();
        
		//end sound
        //button move
        DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		width = metrics.widthPixels;
		height = metrics.heightPixels;
        
		context = getApplicationContext();
		// ***** Facebook part ***		
		mLoginButton = (LoginButton) findViewById(R.id.login);
		mPostButton = (Button) findViewById(R.id.postButton);
		mRankingButton = (Button) findViewById(R.id.ranking);
		test = (Button) findViewById(R.id.sound);
		
		socialNetworkManager = new SocialNetworkManager();
		socialNetworkManager.facebookLogin();
		
		SessionStore.restore(socialNetworkManager.getmFacebook(),this);
        SessionEvents.addAuthListener(new SampleAuthListener());
        SessionEvents.addLogoutListener(new SampleLogoutListener());
        mLoginButton.init(this, socialNetworkManager.getmFacebook());
        socialNetworkManager.loggedWithFacebookUpdate();
        
        
        
      //set userID at start if session is valid
      		if (Utility.mFacebook.isSessionValid()){
      			try{
      	        	JSONObject me = new JSONObject(Utility.mFacebook.request("me"));
      	        	String id = me.getString("id");
      	        	Utility.userUID=id;
      	        	Log.d("trace",id);      	        	
      	        	setFriends();
      	        	Utility.user= new User(1,id);
      	        	}
      			catch(Exception e)
      				{
      				try {
						Utility.mFacebook.logout(getApplicationContext());
					} catch (MalformedURLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
      				Log.d("trace","catch session error "+e.getMessage());}
      		
      				}
      		else{
      			Log.d("trace","session not valid");
      			
      			
      		}
	
	}
	
	
	
	//test to produce a sound, from
	//http://www.droidnova.com/creating-sound-effects-in-android-part-1,570.html
	@TargetApi(13)
	public void sound(View target){
		if (firstClick){
			int[] loca = new int[2];
			target.getLocationOnScreen(loca);
			Xorigin=loca[0];Yorigin=loca[1];
			String str=""+Xorigin+", "+Yorigin;
			Log.d("trace",str);
			firstClick=false;
		}
		
		SoundManager.playSound(1,1);
		//geting size of screen
		/*if (i<5){*/
			int widthButton = test.getWidth();			
			int heightButton = test.getHeight();
			
			//test moving button	
			int[] location = new int[2];
			target.getLocationOnScreen(location);
			X=location[0];Y=location[1];
			//String str="X : "+X+" // largeur bouton "+widthButton+"Largeur tot :"+width;
			//Log.d("trace",str);
			
			//X
			int min =-(int)X;
			int max = width-(int)X-widthButton;
			Random rand = new Random();			
			int finalX = rand.nextInt(max - min + 1) + min;
			
			//Y
			min = -(int)Y+76;
			max = height-(int)Y-heightButton;		
			int finalY = rand.nextInt(max - min + 1) + min;		
			
			//
			String rep = X+" --> "+finalX+" = "+(X+finalX);
			Log.d("trace",rep);
			
			TranslateAnimation slide = new TranslateAnimation(X,X, Y,Y );   
			slide.setDuration(1);   
			slide.setFillAfter(true);   
			
			
			MyAnimationListener listener=new MyAnimationListener(target, finalX,finalY,LogginActivity.this);
			slide.setAnimationListener(listener);
			test.startAnimation(slide); 
			i++;
		/*}
		else{if (i== 5){
			i++;
			putButtonBack(target);
			}
		}*/
		
		
	}
	public void putButtonBack(View target){
		int[] location = new int[2];
		target.getLocationOnScreen(location);
		X=location[0];Y=location[1];
		int finalX=(int)(Xorigin-X);	
		int finalY = (int)(Yorigin-Y);
		TranslateAnimation slide = new TranslateAnimation(X,X, Y,Y );   
		slide.setDuration(1);   
		slide.setFillAfter(true);		
		MyAnimationListener listener=new MyAnimationListener(target, finalX,finalY,LogginActivity.this);
		slide.setAnimationListener(listener);
		test.startAnimation(slide); 		
	}
	
	public void solo(View target){
		
		
		Intent t = new Intent(LogginActivity.this, SingleDuelActivity.class);
		startActivity(t);
	}
	
	
	
	public void rankingActivity(View target){
		
		
		Intent t = new Intent(LogginActivity.this, Ranking.class);
		startActivity(t);
	}
	
	public class SampleAuthListener implements AuthListener {

        @Override
		public void onAuthSucceed() {
            //mText.setText("You have logged in! ");
        	Toast.makeText(context, "You have logged in!", Toast.LENGTH_SHORT).show();
        	socialNetworkManager.loggedWithFacebookUpdate();
        	String id="";
        	String firstName="";
        	try{
        	JSONObject me = new JSONObject(Utility.mFacebook.request("me"));
        	id = me.getString("id");
        	firstName = me.getString("first_name");
        	
        	Log.d("trace",id);
        	Utility.userUID=id;
        	setFriends();
        	
        	
        	}
        	catch(Exception e){
        		Log.d("trace","error getting fb id "+e.getMessage());
        	}
        	
			
        	 String	stringE = "{\"Users\":[{\"ID\":\""+id+"\",\"name\":\""+firstName+"\",\"nickname\":\"pseudo\",\"score\":\"0\"}]}";
        	 if (stringE.equals("1")){
        		 //return one if created in database, so i take same infos
        		 Utility.user = new User(firstName,id,0);
        		 Log.d("trace",stringE);
        	 }else{
        		 //return 0 or error, so User is created by a post
        		 Utility.user = new User(1,id);
        		 Log.d("trace",stringE);
        	 }
        	
        	postData("http://duelos-android.appspot.com/adduser",stringE);
        	//Log.d("trace","post ici ?");
        }
        @Override
		public void onAuthFail(String error) {
            //mText.setText("Login Failed: " + error);
        	Utility.userUID="";
        	
        	Toast.makeText(context, "Login Failed: " + error, Toast.LENGTH_SHORT).show();
        	socialNetworkManager.loggedWithFacebookUpdate();
        }
	}
	
	public class SampleLogoutListener implements LogoutListener {
        @Override
		public void onLogoutBegin() {
            //mText.setText("Logging out...");
        	Toast.makeText(context, "Logging out...", Toast.LENGTH_SHORT).show();
        }

        @Override
		public void onLogoutFinish() {
            //mText.setText("You have logged out! ");
        	Toast.makeText(context, "You have logged out!", Toast.LENGTH_SHORT).show();
        	socialNetworkManager.loggedWithFacebookUpdate();
        	Utility.userUID="";
            
        }
        
        
    }

	public class SampleUploadListener extends BaseRequestListener {

		@Override
		public void onComplete(final String response, final Object state) {
            try {
                // process the response here: executed in background thread
                Log.d("Facebook-Example", "Response: " + response.toString());
                JSONObject json = Util.parseJson(response);
                final String name = json.getString("name");

                // then post the processed result back to the UI thread
                // if we do not do this, an runtime exception will be generated
                // e.g. "CalledFromWrongThreadException: Only the original
                // thread that created a view hierarchy can touch its views."
                LogginActivity.this.runOnUiThread(new Runnable() {
                    @Override
					public void run() {
                    	Toast.makeText(context, "Hello "+name, Toast.LENGTH_SHORT).show();
                    }
                });
            } catch (JSONException e) {
                Log.w("Facebook-Example", "JSON Error in response");
            } catch (FacebookError e) {
                Log.w("Facebook-Example", "Facebook Error: " + e.getMessage());
            }
        }
		
		
    }
	
	
	public void BTActivity(View target){
		
		Intent t = new Intent(LogginActivity.this, MultiDuelActivity.class);
		startActivity(t);
	}
	
	public void postOnWall(View target){
		//test send shot on FC wall
		Disparo disp = new Disparo("user", 1, 2, 3, 4, null);
		
		Utility.mFacebook.authorize(this,new String[] {"publish_stream"}, Facebook.FORCE_DIALOG_AUTH /*new String[] {"publish_stream", "read_stream", "offline_access"}*/,new LoginDialogListener());
		SessionStore.restore(Utility.mFacebook, this);
		socialNetworkManager.postInfo(disp);
		
		
		 // http://stackoverflow.com/questions/12670011/failed-to-find-provider-info-for-com-facebook-katana-provider-attributionidprovi
		 
	}
	
	public class LoginDialogListener implements DialogListener {

		@Override
		public void onComplete(Bundle values) {
			Log.d("trace","on complete lalalala");
			
		}

		@Override
		public void onFacebookError(FacebookError e) {
			Log.d("trace","ON FB ERROR"+e.getMessage());
			
		}

		@Override
		public void onError(DialogError e) {
			Log.d("trace","ON DIALOG ERROR"+e.getMessage());
			
		}

		@Override
		public void onCancel() {
			Log.d("trace","ON ACNCEL ");
			
		}
    }
	
	public void setFriends(View target){
		if (!Utility.mFacebook.isSessionValid()) {
            Util.showAlert(this, "Warning", "You must first log in.");
        } 
		else {            
	        graph_or_fql = "fql";
	        String query = "select name, current_location, uid, pic_square from user where uid in (select uid2 from friend where uid1=me()) order by name";
	        Bundle params = new Bundle();
	        params.putString("method", "fql.query");
	        params.putString("query", query);
	        Utility.mAsyncRunner.request(null, params,new FriendsRequestListenerManager());
        }
	}
	
	public void setFriends(){
		//boolean catchedRaised=false;
		//https://developers.facebook.com/docs/reference/fql/
		if (!Utility.mFacebook.isSessionValid()) {
            //Util.showAlert(this, "Warning", "You must first log in.");
        } 
		else {   
			String ids="";
			String data;
			try {
				data = UtilityServer.getData("http://duelos-android.appspot.com/getUsersId");
				Log.d("trace",data);
				//JSON PART
				JSONObject jsonObj = new JSONObject(data);
				JSONArray jsonArray = jsonObj.getJSONArray("result");
				if(jsonArray.length() != 0)					
					{
					for (int i=0;i<jsonArray.length();i++)
						{ 
						JSONObject temp = jsonArray.getJSONObject(i);
						String id = temp.getString("ID");
						ids+=id+",";
						}
					ids = ids.substring(0, ids.length()-1);
					Log.d("trace","ids : "+ids);
					}
				
			} 
					
			catch(Exception e){Log.d("trace","CATCH RAISED"+e.getMessage()); }
				
			
	        graph_or_fql = "fql";
	        //me and all user from app that are mine fb's friend
	        String query = "select name, current_location, uid, pic_square from user WHERE uid = me() OR (uid IN ("+ids+") AND uid IN (select uid2 from friend where uid1=me())) order by name";
	        Bundle params = new Bundle();
	        params.putString("method", "fql.query");
	        params.putString("query", query);
	        Utility.mAsyncRunner.request(null, params,new FriendsRequestListenerManager());
			
			/*           
		        graph_or_fql = "fql";
		        String query = "select name, current_location, uid, pic_square from user where uid in (select uid2 from friend where uid1=me()) order by name";
		        Bundle params = new Bundle();
		        params.putString("method", "fql.query");
		        params.putString("query", query);
		        Utility.mAsyncRunner.request(null, params,new FriendsRequestListenerManager());
			
			*/
		
        }
	}
	
	public void getFriends(View target){		
	
        if (!Utility.mFacebook.isSessionValid()) {
            Util.showAlert(this, "Warning", "You must first log in.");
        } else {
            dialog = ProgressDialog.show(LogginActivity.this, "",
                    getString(R.string.please_wait), true, true);
            new AlertDialog.Builder(this)
                    .setTitle(R.string.Graph_FQL_title)
                    .setMessage(R.string.Graph_FQL_msg)
                    .setPositiveButton(R.string.graph_button,
                            new DialogInterface.OnClickListener() {
                                 
                                @Override
								public void onClick(DialogInterface dialog, int which) {
                                    graph_or_fql = "graph";
                                    Bundle params = new Bundle();
                                    params.putString("fields", "name, picture, location");
                                    Utility.mAsyncRunner.request("me/friends", params,
                                            new FriendsRequestListener());
                                }

                            })
                    .setNegativeButton(R.string.fql_button,
                            new DialogInterface.OnClickListener() {
                                 
                                @Override
								public void onClick(DialogInterface dialog, int which) {
                                    graph_or_fql = "fql";
                                    String query = "select name, current_location, uid, pic_square from user where uid in (select uid2 from friend where uid1=me()) order by name";
                                    Bundle params = new Bundle();
                                    params.putString("method", "fql.query");
                                    params.putString("query", query);
                                    Utility.mAsyncRunner.request(null, params,
                                            new FriendsRequestListener());
                                }

                            }).setOnCancelListener(new DialogInterface.OnCancelListener() {
                         
                        @Override
						public void onCancel(DialogInterface d) {
                            dialog.dismiss();
                        }
                    }).show();
        }
        
		
	}
	/*
     * callback after friends are fetched via me/friends or fql query.
     */
    public class FriendsRequestListener extends BaseRequestListener {

         
        @Override
		public void onComplete(final String response, final Object state) {
            dialog.dismiss();
            Intent myIntent = new Intent(getApplicationContext(), FriendsList.class);
            myIntent.putExtra("API_RESPONSE", response);
            myIntent.putExtra("METHOD", graph_or_fql);
            startActivity(myIntent);
        }

        public void onFacebookError(FacebookError error) {
            dialog.dismiss();
            Utility.userUID="";
            Toast.makeText(getApplicationContext(), "Facebook Error: " + error.getMessage(),
                    Toast.LENGTH_SHORT).show();
        }
    }
    
    public class FriendsRequestListenerManager extends BaseRequestListener {
    	// put friend list in the socialnetworkmanager without opening an activity
        
        @Override
		public void onComplete(final String response, final Object state) {
            
            try {
            	myJsonArray = new JSONArray(response);
            	socialNetworkManager.setFriends(myJsonArray);
            	//Log.d("trace",socialNetworkManager.getFriends().toString());
            	
            	//Log.d("trace",socialNetworkManager.arrayListFriendtoString());
                
            } catch (JSONException e) {
            	Toast.makeText(LogginActivity.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();                
                return;
            }
        }

        public void onFacebookError(FacebookError error) {
            dialog.dismiss();
            Toast.makeText(getApplicationContext(), "Facebook Error: " + error.getMessage(),
                    Toast.LENGTH_SHORT).show();
        }
    }
    
   
    public void postData(String address, String stringE) {
        // Create a new HttpClient and Post Header
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(address);

        try {
            // Add your data
        	httppost.setHeader("Content-type", "application/json");
        	//Log.d("trace",stringE);
        	httppost.setEntity(new StringEntity(stringE));
        	httppost.setHeader("Accept", "application/json");
            HttpResponse response = httpclient.execute(httppost);
            StringBuilder reponse = inputStreamToString(response.getEntity().getContent());
            String rep = reponse.toString();
            //Log.d("trace",rep);
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
        } catch (IOException e) {
            // TODO Auto-generated catch block
        }
    }  
    
 // Fast Implementation
    private StringBuilder inputStreamToString(InputStream is) throws IOException {
        String line = "";
        StringBuilder total = new StringBuilder();
        
        // Wrap a BufferedReader around the InputStream
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));

        // Read response until the end
        while ((line = rd.readLine()) != null) { 
            total.append(line); 
        }
        
        // Return full string
        return total;
    }
}
