package com.moob;



import java.util.Random;

import com.facebook.android.R;
import com.moob.BT.ClientGame;
import com.moob.BT.UtilityBT;
import com.moob.frwk.MyAnimationListener;
import com.moob.Sound.SoundManager;
import com.moob.frwk.Disparo;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

@TargetApi(11)
public class GameClientActivity extends Activity{
	
	private ClientGame clientGame;
	private Disparo disparo;
	private static Handler handler2;
	private boolean canShootAfterVibration=false;
	private Button nextRound,btnshot;
	
	private int height,width;
	//private int widthButton,heightButton;
	private float X ,Y;
	private int Xorigin,Yorigin;
	private boolean firstClick=true;
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);								
		setContentView(R.layout.client_game);
		nextRound = (Button) findViewById(R.id.nextRound);
		btnshot = (Button) findViewById(R.id.btnshot);
		
		
		nextRound.setVisibility(View.GONE);
		
		//moving button
			DisplayMetrics metrics = new DisplayMetrics();
			getWindowManager().getDefaultDisplay().getMetrics(metrics);
			width = metrics.widthPixels;
			height = metrics.heightPixels;
			
				
		handler2 = new Handler(){
            @Override
            public void handleMessage(Message msg) {
            	Log.d("trace","clientGameActivity handlerMessage");
            	switch(msg.what)
            	  {
            	   case 0:
            		   Log.d("trace","ClientGame case 0");            		  
            		   // Get instance of Vibrator from current Context
            		   Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);            		    
            		   // Vibrate for 300 milliseconds
            		   canShootAfterVibration=true;            		  
            		   v.vibrate(1000);            		   
            		   // wait randon seconds
            		   
            		   
            	   break;
            	   
            	   case 2:
            		   //I have been shooted
            		   Toast.makeText(getApplicationContext(),"click to start next round",Toast.LENGTH_SHORT).show(); 
            		   nextRound.setVisibility(View.VISIBLE);            		   
            	   
            	   case 1:
            		   Log.d("trace","ClientGame case 1");
            		   String user = (String)msg.obj;
            		   if (user.equals("fake")){
            			   //i won and he pressed next round
            			   
            		   }
            		   else{
            			   // I lost
            			   SoundManager.playSound(2,1);
            			   Toast.makeText(getApplicationContext(),user+" shoot you",
          	  			          Toast.LENGTH_SHORT).show(); 
            		   }
            	   break;
            	   
            	   case 4:
            		   canShootAfterVibration=false;
            	   break;
            	  }         
            }
        };
        
        try{
    		clientGame = new ClientGame(UtilityBT.mDevice);
    		clientGame.setHandler(handler2);
    		
    		}
    		catch(Exception e){
    			Log.d("trace","ERROR GameClientActivity  : "+e.getMessage());
    		}
    		
    		try{
    		clientGame.start();
    		Log.d("trace","ClientGame Started : ");
    		}
    		catch(Exception e){
    			Log.d("trace","ERROR GameClientActivity getSerializableExtra : "+e.getMessage());
    		}
        
	}
	
	public void nextRound(View v){
		//go next round
		nextRound.setVisibility(View.GONE);
		
		 synchronized(clientGame) {
			 clientGame.notify();
	        }		
	}
	
	public void putButtonBack(View target){
		
		int[] location = new int[2];
		target.getLocationOnScreen(location);
		X=location[0];Y=location[1];
		int finalX=(int)(Xorigin-X);	
		int finalY = (int)(Yorigin-Y);
		TranslateAnimation slide = new TranslateAnimation(X,X, Y,Y );   
		slide.setDuration(1);   
		slide.setFillAfter(true);		
		MyAnimationListener listener=new MyAnimationListener(target, finalX,finalY,GameClientActivity.this);
		slide.setAnimationListener(listener);
		btnshot.startAnimation(slide); 		
	}
	
	public void onClickShot(View v){
		
		
		if (firstClick){
			int[] loca = new int[2];
			v.getLocationOnScreen(loca);
			Xorigin=loca[0];Yorigin=loca[1];
			firstClick=false;
		}
		
		if (clientGame.isAlive() && canShootAfterVibration){
			
			SoundManager.playSound(1,1);
			
			disparo = new Disparo("user", 0, 0, 0, 0, null);
			clientGame.setSentShoot(true);
			clientGame.sendMessage(disparo);
			
			//You shooted first
			//upgrade score etc
			
			canShootAfterVibration=false;
			int[] location = new int[2];
			btnshot.getLocationOnScreen(location);
			X=location[0];
			if (X != Xorigin)
				putButtonBack(v);
			
			 
		}
		else{
			// moving button	
			int[] location = new int[2];
			btnshot.getLocationOnScreen(location);
			X=location[0];Y=location[1];
			
			int widthButton = v.getWidth();			
			int heightButton = v.getHeight();
			
			//X
			int min =-(int)X;
			int max = width-(int)X-widthButton;
			Random rand = new Random();			
			int finalX = rand.nextInt(max - min + 1) + min;
			
			//Y
			min = -(int)Y+76;//32*2 for status bar and app bar
			max = height-(int)Y-heightButton;			
			int finalY = rand.nextInt(max - min + 1) + min;
			
			String rep = X+" --> "+finalX+" = "+(X+finalX)+" , button largeur"+widthButton+" ,largeur ecran"+width;
			Log.d("trace",rep);
			
			//translation does not translate, it's the listener  on animation end who does
			TranslateAnimation slide = new TranslateAnimation(X,X, Y,Y );   
			slide.setDuration(1);   
			slide.setFillAfter(true);   
			
			MyAnimationListener listener=new MyAnimationListener(v, finalX,finalY,GameClientActivity.this);
			slide.setAnimationListener(listener);
			btnshot.startAnimation(slide); 
		
			
		}
	}
	public void cancel(View v){
		
		try {clientGame.stopServeur(v);}
		catch(Exception e){};
		finish();
	}
	
	
	@Override
	protected void onDestroy() {
	  super.onDestroy();
	  clientGame.stopServeur();
	  
	}

}
