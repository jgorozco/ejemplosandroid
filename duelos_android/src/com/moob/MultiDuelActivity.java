package com.moob;


import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Set;

import org.json.JSONObject;

import com.facebook.android.R;
import com.moob.BT.ClientGame;
import com.moob.BT.ListServerAdapter;
import com.moob.BT.UtilityBT;
import com.moob.FB.Utility;
import com.moob.frwk.Server;
import com.moob.frwk.User;
import com.moob.frwk.UtilityServer;

import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class MultiDuelActivity extends Activity {
	
    private Button mClientButton,mServerButton,mBroadcast,mBroadcastConnect;
    private ListView mServerList;
    private ListServerAdapter adapter;
    int REQUEST_DISCOVERABLE_BT=1;
    BluetoothDevice device=null;
    BluetoothAdapter bluetoothAdapter=null;
    private Handler handlerConnection,handlerListServer;
    //client variables
    private User user;
    
    private Set<BluetoothDevice> devices;
    //private ArrayList<HashMap<String, String>> listServer;//BT devices in HashMap for listView
    private ArrayList<BluetoothDevice> mesDevices;
    private ArrayList<BluetoothDevice> mesDevicesBroadcast;
    private ConnectThread client;
    private ClientGame clientGame;
    private String reponse = "vide";
    private ArrayList<Server> servers;
    //http://developer.android.com/about/dashboards/index.html
    private int sdkVersion = 0;
    
    
    
    
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.multi_duel);
		mClientButton = (Button) findViewById(R.id.client);
		mServerButton = (Button) findViewById(R.id.server);
		mBroadcast = (Button) findViewById(R.id.broadcast);
		mBroadcast.setVisibility(View.GONE);
		mBroadcastConnect = (Button) findViewById(R.id.broadcastConnect);
		mBroadcastConnect.setVisibility(View.GONE);
		mServerList = (ListView) findViewById(R.id.serverList);
		servers = new ArrayList<Server>();
		adapter = new ListServerAdapter(this,servers);
		mServerList.setAdapter(adapter);
		//User
		if (Utility.userUID.equals("")){
			user = new User("UserWithoutInternetConnection");
			}
		else{
			String userID = Utility.userUID;
			String url = "http://duelos-android.appspot.com/getUser";
			//post to data base to get user info
			String jsonSent="{\"userId\":\""+userID+"\"}";
			String data = UtilityServer.postData(url,jsonSent);
			try {
				JSONObject jsonObj = new JSONObject(data);
				try{
					String error = jsonObj.getString("error");
					}
				catch(Exception e){
					Log.d("trace","request didn't send error");
					try{
						String nickName = jsonObj.getString("nickName");
						String name = jsonObj.getString("name");
					
						int score = jsonObj.getInt("score");
						
						user = new User(name,Utility.userUID,score);
						Log.d("trace",user.toString());
					}
					catch(Exception e2){Log.d("trace",e2.getMessage());}
						
					}
				}
			catch(Exception e3){Log.d("trace",e3.getMessage());}
			
		}
		
		//test if bluetooth
		mesDevicesBroadcast = new ArrayList<BluetoothDevice>();
		
		bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            // the terminal does not have BlueTooth
        	Log.d("trace","no bluetooth on device");
        	Toast.makeText(getApplicationContext(),"NO BLUETOOTH ON DEVICE",
			          Toast.LENGTH_SHORT).show();
        	this.finish();
        	
        }
        else{
        	beVisible();
        	
        	handlerListServer = new Handler(){
                 @Override
                 public void handleMessage(Message msg) {
                 	Server server = (Server)msg.obj;
                 	Log.d("trace","HANDLER"+server.toString());
                 	servers.add(server);
                 	Log.d("trace","HANDLER SIZE : "+servers.size());
                 	adapter.notifyDataSetChanged();
                 	
                 }
             };
        }
        //http://developer.android.com/reference/android/bluetooth/BluetoothAdapter.html
        //http://stackoverflow.com/questions/7863019/android-bluetooth-action-discovery-finished-not-working
        IntentFilter filter = new IntentFilter();
        
        filter.addAction(BluetoothDevice.ACTION_FOUND);//detection of BT device
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED); // detection of the end of the broadcast
       
        this.registerReceiver(bluetoothReceiver, filter);     
        
        //version of android OS
        sdkVersion = android.os.Build.VERSION.SDK_INT; // e.g. sdkVersion := 8;
        Log.d("trace","sdkVersion : "+sdkVersion);
    	

        mServerList.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				
				
				 Toast.makeText(getApplicationContext(),"tocado:"+ String.valueOf(arg2),
				          Toast.LENGTH_SHORT).show();
				 
				 if (bluetoothAdapter != null)
				 	{
					 BluetoothDevice maDevice=null;
					 Server myServer = servers.get(arg2);
					 for (BluetoothDevice mDevice : mesDevices){
						 if(mDevice.getAddress().equals(myServer.getAddress())){
							 maDevice = mDevice;
							 Log.d("trace","DEVICE MATCH (known devices) : "+maDevice.getName());
						 }
					 }
					 //device not in known BT devices
					 if(maDevice == null){
						 for (BluetoothDevice mDevice : mesDevicesBroadcast){
							 if(mDevice.getAddress().equals(myServer.getAddress())){
								 maDevice = mDevice;
								 Log.d("trace","DEVICE MATCH (broadcast devices) : "+maDevice.getName());
							 }
						 }
					 }
					 
					 if(maDevice != null){
						Intent t = new Intent(MultiDuelActivity.this, GameClientActivity.class);
						UtilityBT.mDevice=maDevice;						
						startActivity(t);
						
					 }
				 }
				
			}
        	
		});
        
	}
	
	public void updateView()
	{
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Log.d("trace", "ON updateView");
				adapter.notifyDataSetChanged();
				
				
			}
		});
	}
	
	public void server(View v){
		if (!bluetoothAdapter.isEnabled()) 
    		bluetoothAdapter.enable();		
		
		Intent t = new Intent(MultiDuelActivity.this, LoadingActivity.class);		
		startActivity(t);
	}

	public void client(View v){
		mBroadcast.setVisibility(View.GONE);
		mBroadcastConnect.setVisibility(View.GONE);
		Log.d("trace","init array");
		//listServer = new ArrayList<HashMap<String,String>>();
		mesDevices = new ArrayList<BluetoothDevice>();
		
		//1 load all known devices 
		getKnownBTDevice();
		 
		
		
		//2 try do connect to one of them
		for(int i=0; i<mesDevices.size();i++){
			reponse="wait";
			Log.d("trace","DEVICE NAME BEFORE INTENT CONNECTION : "+mesDevices.get(i).getName());
			intentBTconnexion(mesDevices.get(i));
			while(reponse == "wait"){ 
				
			}
		updateView();	
		Log.d("trace","reponse : "+reponse);	
		}		
		
		//3 Load all Bluetooth close devices
		
		
		
		//4 try to connect to one of them
			
		//5 message connection fail
		if (mesDevices.size() == 0){
			Toast.makeText(getApplicationContext(),"NO SERVER FOUND",
			          Toast.LENGTH_SHORT).show();
		
				}
		mBroadcast.setVisibility(View.VISIBLE);
		
		// show a list of all BT device to try to connect manually on click
	
	}
	
	private void intentBTconnexion(BluetoothDevice device){
		//try to connect to bluetooth devices, if connection is established
		//then add the specific server in the list of server
		
		if (bluetoothAdapter != null){
					
				client = new ConnectThread(device);
				client.start();
		}
		else{
			Log.d("trace","Else intentBTconnexion");
			reponse="error";
		}
		
	}
	
	private void getKnownBTDevice() {
		//http://developer.android.com/reference/android/bluetooth/BluetoothClass.html
		//http://developer.android.com/reference/android/bluetooth/BluetoothClass.Device.html#PHONE_SMART
		
		if (!bluetoothAdapter.isEnabled()) 
    		bluetoothAdapter.enable();        	
    	//Obtenir la liste des devices d�j� connus
    	devices = bluetoothAdapter.getBondedDevices();
    	// If there are paired devices
    	
    	if (devices.size() > 0) {
    	    // Loop through paired devices
    	    for (BluetoothDevice device : devices) {
    	    	//if my device is a smart phone
    	    	if (device.getBluetoothClass().getDeviceClass() ==  BluetoothClass.Device.PHONE_SMART)
    	    		{
	    	        // Add the name and address to an array adapter to show in a ListView
	    	    	Toast.makeText(getApplicationContext(),"BT device : "+device.getName(),
	  			          Toast.LENGTH_SHORT).show();
	    	    	   
	    	    	mesDevices.add(device);
	    	    	
	    	    	Log.d("trace","known device added");
    	    		}
    	   		}
    		}
    	Log.d("trace","mesDevices SMARTPHONE know : "+mesDevices.size());
	}
	/*
	private void addListadoData(BluetoothDevice device){
		HashMap<String, String> map=new HashMap<String, String>();        	    	
    	map.put("nom", device.getName());
    	map.put("address", device.getAddress());    	
    	listServer.add(map); 
    	
	}*/
	
	//D�couverte des terminaux Bluetooth visibles et proches
	public void broadcast(View v){
		mBroadcastConnect.setVisibility(View.GONE);
		
		bluetoothAdapter.startDiscovery();//start  BroadcastReceiver on Receive
		
		Log.d("trace","fonction broadcast");
		
		
		
		
	}
	
	public void connectAfterBroadcast(View v){
		mBroadcastConnect.setVisibility(View.VISIBLE);
		
		Log.d("trace","mesDevicesBroadcast size : "+ mesDevicesBroadcast.size());
		for(int i=0; i<mesDevicesBroadcast.size();i++){
			reponse="wait";
			Log.d("trace","DEVICE NAME BEFORE INTENT CONNECTION : "+mesDevicesBroadcast.get(i).getName());
			intentBTconnexion(mesDevicesBroadcast.get(i));
			while(reponse == "wait"){ 
				
			}
			
		Log.d("trace","reponse : "+reponse);	
		}		
		
		
	}
	
	private final BroadcastReceiver bluetoothReceiver = new BroadcastReceiver() {
		
			@Override
			public void onReceive(Context context, Intent intent) {
				Log.d("trace"," broadcast ...");
				String action = intent.getAction();
				//Log.d("trace","start broadcast onReceive");
				if (BluetoothDevice.ACTION_FOUND.equals(action)) 
					{
					BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
					//if my device is a smart phone
					if (device.getBluetoothClass().getDeviceClass() ==  BluetoothClass.Device.PHONE_SMART)
						
						{
						Toast.makeText(MultiDuelActivity.this, "New Device = " + device.getName(), Toast.LENGTH_SHORT).show();
						Log.d("trace","New Device form broadcast = " + device.getName());
						
						
		    	    	//add the device found at the list
						mesDevicesBroadcast.add(device);
		    	    	
						}
					else{
						//add the device found at the list
						Log.d("trace","New Device form broadcast BUT NO SMARTPHONE = " + device.getName());
					}
					
					}
				
				if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)){				
				Log.d("trace","ACTION_DISCOVERY_FINISHED");
				connectAfterBroadcast(mBroadcast);
				}
				
				
				}
			};
	
	public void beVisible(){    	
    	
    	//Demande � l'utilisateur l'autorisation pour rendre visible le terminal
    	Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
    	discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 90); // Cette ligne permet de d�finir une dur�e de visibilit� de notre choix
    	startActivityForResult(discoverableIntent, REQUEST_DISCOVERABLE_BT);
		Log.d("trace","apres demande autorisation terminal VISIBLE");
		
    }
	@Override
	protected void onDestroy() {
	  super.onDestroy();
	  if (bluetoothAdapter != null){
	  bluetoothAdapter.cancelDiscovery();
	  }
	  unregisterReceiver(bluetoothReceiver);
	  
	}
	
	//******************* CLIENT LIST VIEW *********************
    
	private class ConnectThread extends Thread {
    	   private final BluetoothSocket mmSocket;
    	   private final BluetoothDevice mmDevice;
    	   private ObjectOutputStream out;
    	   private ObjectInputStream in;
    	   private String message;
    	   private boolean open = false;
    	   private  long startTime,stopTime;    	   
		
		public ConnectThread(BluetoothDevice device) {
    	       BluetoothSocket tmp = null;
    	       mmDevice = device;
    	       InputStream tmpIn = null;
               OutputStream tmpOut = null;
               
    	       try {
    	    	   //http://stackoverflow.com/questions/5885438/bluetooth-pairing-without-user-confirmation
    	    	   // Insecure connection with Android 2.3.3 and further
    	    	   if (sdkVersion>8) //2.3 and more
    	           tmp = socketSDKversion8plus(device);
    	    	   else
    	    	   tmp = socketSDKversion8andLess(device);
    	       } 
    	       catch (IOException e) 
	    	       	{
	    	    	Log.d("trace","CLIENT thread error"+e.getMessage()); 
	    	    	}
    	       mmSocket = tmp;
    	   }
    	   @TargetApi(10)
    	   public BluetoothSocket socketSDKversion8plus(BluetoothDevice device) throws IOException{
    		   BluetoothSocket tmp = null;
    		   tmp = device.createInsecureRfcommSocketToServiceRecord(Utility.uuid);
    		   return tmp;
    	   }
    	   public BluetoothSocket socketSDKversion8andLess(BluetoothDevice device) throws IOException{
    		   BluetoothSocket tmp = null;
    		   tmp = device.createRfcommSocketToServiceRecord(Utility.uuid);
    		   return tmp;
    	   }
    	   @Override
		public void run()  {
    		      		   
    		   bluetoothAdapter.cancelDiscovery();
    		   Log.d("trace","Cancel discovery");
    	       try {
    	    	   Log.d("trace","socket client try connect "+this.getClass().getName());
    	           mmSocket.connect() ;
    	           
    	       } catch (IOException connectException) {
    	    	   
    	           try {
    	               mmSocket.close();
    	               Log.d("trace","socket client close : "+connectException.getMessage());
    	               reponse="fail";
    	               
    	               
    	           } catch (IOException closeException) {
    	        	   	Log.d("trace","CLIENT close Error"+closeException.getMessage());
    	        	   	reponse="fail2";
    	           		}
    	           
    	           Log.d("trace","IOException : "+connectException.getMessage());
    	           reponse="error";
    	           return;
    	       }
    	       Log.d("trace","apres le try connect");
    	       reponse="test";
    	       try{
    	     	//2. get Input and Output streams
  	   			out = new ObjectOutputStream(mmSocket.getOutputStream());
  	   			out.flush();
  	   			in = new ObjectInputStream(mmSocket.getInputStream());
  	   			Log.d("trace","CLIENT CONNECTE debut fonction");  	   		
  	   			manageConnectedSocket(mmSocket);
    	       }
    	       catch(Exception e){
    	    	   Log.d("trace","IOSTREAM ERROR"+e.getMessage());
    	    	   reponse="error";
    	       }
    	       reponse="success";
    	   }
    	   private void manageConnectedSocket(BluetoothSocket mmSocket) {
    		   //CLIENT
    		   //1 send a String
    		   //
    		   open = true;
    		   startTime = System.currentTimeMillis();
    		   sendMessage("detection");
    		   
    		   while (open){
   				//message = (String)in.readObject();
				//Log.d("trace","server>" + message);
				
				try {
					Log.d("trace","try lecture server object");
					Server server = (Server)in.readObject();
					stopTime = System.currentTimeMillis();
					long ping = (stopTime - startTime);
					server.setPing(ping);
					Log.d("trace","server recu PING : "+ping);
					Log.d("trace","server recu : "+server.toString());
					Message msg = new Message();
					msg.obj=server;					
					handlerListServer.sendMessage(msg);
					open=false;
				} catch (OptionalDataException e) {
					
					Log.d("trace","erreur1"+e.getMessage());
					open=false;
				} catch (ClassNotFoundException e) {
					open=false;
					Log.d("trace","erreur2"+e.getMessage());
				} catch (IOException e) {
					open=false;
					Log.d("trace","erreur3"+e.getMessage());
				}
				
   			}
    		   
    		  
    		   
     }
    	   
	   public void sendMessage(String msg){    		
			try{
				out.writeObject(msg);
				out.flush();
				Log.d("trace","client envois >" + msg);
			}
			catch(IOException ioException){
				Log.d("trace","client ERROR SEND MESSAGE>"+ioException.getMessage());
			}
		}
	   //http://docs.oracle.com/javase/tutorial/essential/io/objectstreams.html           
	   public void sendMessage(Object obj){ 
   //http://developer.android.com/reference/java/io/ObjectOutputStream.html#write(byte[], int, int)
	try{
		out.writeObject(obj);
		out.flush();
		Log.d("trace","client envois byte[] long : "+obj.toString());
	}
	catch(IOException ioException){
		Log.d("trace","client ERROR SEND MESSAGE>"+ioException.getMessage());
	}
}		
	   
	   public void cancel() {
			open = false;
    	       try {
    	    	   out.close();
    	    	   in.close();
    	    	   mmSocket.close();
    	           Log.d("trace","client close succes");
    	       } catch (IOException e) { 
    	    	   Log.d("trace","CLIENT close (cancel) Error"+e.getMessage());
    	       }
    	   }

    }	
  
  //******************* END CLIENT *********************
}
