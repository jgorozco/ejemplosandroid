package com.moob;

import java.util.Random;

import com.moob.R;
import com.moob.BT.AcceptThread;
import com.moob.Sound.SoundManager;
import com.moob.frwk.Disparo;
import com.moob.frwk.MyAnimationListener;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class LoadingActivity extends Activity{
	private AcceptThread serveur;	
	private Button cancel,btnshot,nextRound;
	private TextView reponse;
	private Handler handler;
	private ProgressBar progressBar ;
	private String string=null;
	private Disparo disparo=null;
	private boolean canShootAfterVibration=false;
	//moving button
	private int height,width;
	
	private float X,Y;
	private int Xorigin,Yorigin;
	private boolean firstClick=true;
	
	@Override
    public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
        setContentView(R.layout.loading);
        
        //moving button
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		width = metrics.widthPixels;
		height = metrics.heightPixels;        
        cancel = (Button)findViewById(R.id.cancel);
        btnshot = (Button)findViewById(R.id.btnshot);
        nextRound = (Button)findViewById(R.id.nextRound);
        btnshot.setVisibility(View.GONE);
        nextRound.setVisibility(View.GONE);
        reponse = (TextView)findViewById(R.id.reponse);
        progressBar = (ProgressBar)findViewById(R.id.progressBar1);        
        
        
        handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
            	
            	switch(msg.what)
            	  {
            	   case 0://String message detection
            		   string = (String)msg.obj;
            		   reponse.setText(string);
            		   Toast.makeText(getApplicationContext(),"Client detected",
       	  			          Toast.LENGTH_SHORT).show();
            	   break;

            	   case 1://stop the server and restart it to wait the string message start game
            		   // but if a client is detecting again, it will detect the string "detection"
            		   serveur.stopServeur();
            		   Toast.makeText(getApplicationContext(),"Waiting for connection",
        	  			          Toast.LENGTH_SHORT).show();
            		   
            		   serveur = new AcceptThread();
            		   serveur.setHandler(handler);
            		   serveur.start();
            		   
            	   break;  
            	   
            	   case 2:
            		   //starting game
            		   string = (String)msg.obj;
            		   reponse.setText(string);
            		   Toast.makeText(getApplicationContext(),string,
        	  			          Toast.LENGTH_SHORT).show();       
            		   //degriser le cercle de chargement ici
            		   progressBar.setVisibility(View.GONE);
            		   btnshot.setVisibility(View.VISIBLE);
            		  
            	   break;
            	   
            	   case 3:
            		   // Get instance of Vibrator from current Context
            		   Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            		   
            		   
            		   // Vibrate for 300 milliseconds
            		   canShootAfterVibration=true;
            		   
            		   v.vibrate(1000);
            		   // wait randon seconds
            		  
            	   break;
            	   
            	   case 4:
            		   canShootAfterVibration=false;
            		   nextRound.setVisibility(View.VISIBLE);
            	   break;
            	   
            	   case 5:
            		   serveur.stopServeur();
            		  
            	   break;
            	   
            	   case 6:
            		   String str= (String)msg.obj;
            		   if (str.equals("fake")){
            			   
            		   }
            		   else{
            			   //death sound
            			   SoundManager.playSound(2,1);
            		   }
            	   break;
            	  }         
            	
            }
        };
        serveur = new AcceptThread();
        serveur.setHandler(handler);
        serveur.start();
        	 
	}
	
	public void putButtonBack(View target){
		Log.d("trace","put button back");
		int[] location = new int[2];
		target.getLocationOnScreen(location);
		X=location[0];Y=location[1];
		int finalX=(int)(Xorigin-X);	
		int finalY = (int)(Yorigin-Y);
		TranslateAnimation slide = new TranslateAnimation(X,X, Y,Y );   
		slide.setDuration(1);   
		slide.setFillAfter(true);		
		MyAnimationListener listener=new MyAnimationListener(target, finalX,finalY,LoadingActivity.this);
		slide.setAnimationListener(listener);
		btnshot.startAnimation(slide); 		
	}
	
	public void nextRound(View v){
		nextRound.setVisibility(View.GONE);
		synchronized(serveur) {
			 serveur.notify();
	        }
	}
	
	public void onClickShot(View v){
		
		if (firstClick){
			int[] loca = new int[2];
			v.getLocationOnScreen(loca);
			Xorigin=loca[0];Yorigin=loca[1];
			firstClick=false;
		}
		
		if (serveur.isAlive() && canShootAfterVibration){
			
			SoundManager.playSound(1,1);
			
			disparo = new Disparo("user", 0, 0, 0, 0, null);
			serveur.setSentShoot(true);
			serveur.sendMessage(disparo);
			
			canShootAfterVibration=false;
			nextRound.setVisibility(View.GONE);

			int[] location = new int[2];
			btnshot.getLocationOnScreen(location);
			X=location[0];
			if (X != Xorigin)
				putButtonBack(v);
		}
		else{
			
			int widthButton = v.getWidth();			
			int heightButton = v.getHeight();
			
			// moving button	
			int[] location = new int[2];
			btnshot.getLocationOnScreen(location);
			X=location[0];Y=location[1];
			//X
			int min =-(int)X;
			int max = width-(int)X-widthButton;
			Random rand = new Random();			
			int finalX = rand.nextInt(max - min + 1) + min;
			
			//Y
			min = -(int)Y+76;//32*2 for status bar and app bar
			max = height-(int)Y-heightButton;			
			int finalY = rand.nextInt(max - min + 1) + min;
			
			//translation does not translate, it's the listener  on animation end who does
			TranslateAnimation slide = new TranslateAnimation(X,X, Y,Y );   
			slide.setDuration(1);   
			slide.setFillAfter(true);   
			
			MyAnimationListener listener=new MyAnimationListener(v, finalX,finalY,LoadingActivity.this);
			slide.setAnimationListener(listener);
			btnshot.startAnimation(slide); 
		}
	}
	
	public void cancel(View v){
		
		try {serveur.stopServeur(v);}
		catch(Exception e){};
		finish();
	}
}
