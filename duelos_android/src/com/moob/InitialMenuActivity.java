package com.moob;

import java.text.DateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import com.moob.DaoMaster.DevOpenHelper;
import com.moob.FB.Example;

public class InitialMenuActivity extends Activity {
private static final String TAG = "TESTING";

	//menu para ir a configuracion, jugar solo, ranking, puntuacion y multplayer
/*
 * 
 * 
 * http://greendao-orm.com/
 * 
 * 
 * */

	private SQLiteDatabase db;
    private DaoMaster daoMaster;
    private DaoSession daoSession;
    private LogroDao daoLogro;
    private Cursor cursor;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mainmenu);
		
		
		
	
	}
	public void clickUno(View target)
	{
		Log.d(TAG, "click 1");	
		DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "notes-db", null);
        db = helper.getWritableDatabase();
        daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();
        daoLogro = daoSession.getLogroDao();
        final DateFormat df = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
        String comment = "logro en:" + df.format(new Date());
        daoLogro.insert(new Logro(null,comment,new Date()));
     /*   List<Logro> logros=daoLogro.queryBuilder().list();
        for (Iterator iterator = logros.iterator(); iterator.hasNext();) {
			Logro logro = (Logro) iterator.next();
			Log.d(TAG, "Logro::"+logro.getLogro_name()+"-"+String.valueOf(logro.getDate()));
		}
        */
        
        
		
	}
	public void clickDos(View target)
	{
		Log.d(TAG, "click 2");
		List<Logro> logros=daoLogro.queryBuilder().list();
		for (Iterator iterator = logros.iterator(); iterator.hasNext();) {
			Logro logro = (Logro) iterator.next();
			Log.d(TAG, "Logro::"+logro.getLogro_name()+"-"+String.valueOf(logro.getDate()));
		}
		
	}
	
	public void exempleFB(View target){
		
		Intent t = new Intent(InitialMenuActivity.this, Example.class);
		startActivity(t);
	}
	
	public void logginActivity(View target){
		
		Intent t = new Intent(InitialMenuActivity.this, LogginActivity.class);
		startActivity(t);
	}
	
	public void rankingActivity(View target){
		//if (Utility.arrayListFriend != null){
		Intent t = new Intent(InitialMenuActivity.this, Ranking.class);
		startActivity(t);
	}

	public void singleDuelActivity(View target){
		
		Intent t = new Intent(InitialMenuActivity.this, SingleDuelActivity.class);
		startActivity(t);
	}

	public void multiDuelActivity(View target){
	
	Intent t = new Intent(InitialMenuActivity.this, MultiDuelActivity.class);
	startActivity(t);
}
}
