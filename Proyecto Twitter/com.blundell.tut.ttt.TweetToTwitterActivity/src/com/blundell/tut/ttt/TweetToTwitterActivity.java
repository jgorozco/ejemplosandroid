package com.blundell.tut.ttt;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.Toast;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

public class TweetToTwitterActivity extends Activity {

	private static final String TAG = "TweetToTwitterActivity";

	private static final String PREF_ACCESS_TOKEN = "accessToken";
	private static final String PREF_ACCESS_TOKEN_SECRET = "accessTokenSecret";
	private static final String CONSUMER_KEY = "5NsnDdVcUMAmhIEGhKhSw";
	private static final String CONSUMER_SECRET = "cvYNRY6A69qBsg14LqOQGIYjI52J8k4nv1LcHOg"; 
	private static final String CALLBACK_URL = "tweet-to-twitter-blundell-01-android:///";
	private SharedPreferences mPrefs;
	private Twitter mTwitter;
	private RequestToken mReqToken;

	private Button mLoginButton;
	private Button mTweetButton;
	private Button reintentar;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.i(TAG, "Loading");
		setContentView(R.layout.activity_main);

		mPrefs = getSharedPreferences("twitterPrefs", MODE_PRIVATE);
		Log.i(TAG, "Got Preferences");

		mTwitter = new TwitterFactory().getInstance();
		Log.i(TAG, "Got Twitter4j");

		mTwitter.setOAuthConsumer(CONSUMER_KEY, CONSUMER_SECRET);
		Log.i(TAG, "Inflated Twitter4j");

		mLoginButton = (Button) findViewById(R.id.login_button);
		mTweetButton = (Button) findViewById(R.id.tweet_button);
		reintentar = (Button) findViewById(R.id.button1);

	}


	public void buttonLogin(View v) {
		Log.i(TAG, "Login Pressed");
		if (mPrefs.contains(PREF_ACCESS_TOKEN)) {
			Log.i(TAG, "Repeat User");
			loginAuthorisedUser();
		} 
		else {
			Log.i(TAG, "New User");
			loginNewUser();
		}
	}

	public void buttonTweet(View v) {
		Log.i(TAG, "Tweet Pressed");
		tweetMessage();
	}


	private void loginNewUser() {
		try {
			Log.i(TAG, "Request App Authentication");
			mReqToken = mTwitter.getOAuthRequestToken(CALLBACK_URL);

			Log.i(TAG, "Starting Webview to login to twitter");
			WebView twitterSite = new WebView(this);
			twitterSite.loadUrl(mReqToken.getAuthenticationURL());
			setContentView(twitterSite);

		} catch (TwitterException e) {
			Toast.makeText(this, "Twitter Login error, try again later", Toast.LENGTH_SHORT).show();
		}
	}


	private void loginAuthorisedUser() {
		String token = mPrefs.getString(PREF_ACCESS_TOKEN, null);
		String secret = mPrefs.getString(PREF_ACCESS_TOKEN_SECRET, null);

		AccessToken at = new AccessToken(token, secret);

		mTwitter.setOAuthAccessToken(at);

		Toast.makeText(this, "Welcome back", Toast.LENGTH_SHORT).show();

		//	enableTweetButton();
	}


	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		Log.i(TAG, "New Intent Arrived");
		dealWithTwitterResponse(intent);
	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.i(TAG, "Arrived at onResume");
	}


	private void dealWithTwitterResponse(Intent intent) {
		Uri uri = intent.getData();
		if (uri != null && uri.toString().startsWith(CALLBACK_URL)) { // If the user has just logged in
			String oauthVerifier = uri.getQueryParameter("oauth_verifier");

			authoriseNewUser(oauthVerifier);
		}
	}


	private void authoriseNewUser(String oauthVerifier) {
		try {
			AccessToken at = mTwitter.getOAuthAccessToken(mReqToken, oauthVerifier);
			mTwitter.setOAuthAccessToken(at);

			saveAccessToken(at);

			setContentView(R.layout.activity_main);

		} catch (TwitterException e) {
			Toast.makeText(this, "Twitter auth error x01, try again later", Toast.LENGTH_SHORT).show();
		}
	}




	private void tweetMessage() {
		try {
			Log.d(TAG, "Twitteando...");
			int x = new Double(Math.random() * 100).intValue();

			Status status = mTwitter.updateStatus("Test - Tweeting with Android " + x);
			Log.d("status", status.toString());


			Toast.makeText(this, "Tweet Successful!", Toast.LENGTH_SHORT).show();
		} catch (TwitterException e) {
			reintentar.setVisibility(View.VISIBLE);
			Log.d("Status code", "  " +e.getStatusCode());
			switch (e.getStatusCode()){
			case 400: 	
				Toast.makeText(this, "Bad Request", Toast.LENGTH_SHORT).show();
				break;
			case 403:
				Toast.makeText(this, "Forbidden", Toast.LENGTH_SHORT).show();
				break;
			case 404:
				Toast.makeText(this, "Not Found", Toast.LENGTH_SHORT).show();
				break;
			case 406:
				Toast.makeText(this, "Not Acceptable", Toast.LENGTH_SHORT).show();
				break;
			case 503:
				Toast.makeText(this, "Service Unavailable", Toast.LENGTH_SHORT).show();
				break;
			default:
				Toast.makeText(this, "Tweet error", Toast.LENGTH_SHORT).show();

			}
		}
	}

	private void saveAccessToken(AccessToken at) {
		String token = at.getToken();
		String secret = at.getTokenSecret();
		Editor editor = mPrefs.edit();
		editor.putString(PREF_ACCESS_TOKEN, token);
		editor.putString(PREF_ACCESS_TOKEN_SECRET, secret);
		editor.commit();
	}
}